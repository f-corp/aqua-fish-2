extends Control


onready var title = $PanelOuter/MarginContainer/PanelInner/MarginContainer/VBoxContainer/Attribute_name
onready var mean_value_text = $PanelOuter/MarginContainer/PanelInner/MarginContainer/VBoxContainer/VBoxContainer/Mean_value
onready var back_panel = $PanelOuter/MarginContainer/PanelInner/MarginContainer/VBoxContainer/VBoxContainer/Panel


var time_check = 0

var is_mouse_on_window = false
var is_mouse_dragged = false
var dragged_point = Vector2.ZERO


func _input(event):
	if event.is_action_pressed("left_click") and is_mouse_on_window:
		dragged_point = get_global_mouse_position() - rect_global_position
		is_mouse_dragged = true

	if event.is_action_released("left_click") and is_mouse_dragged:
		is_mouse_dragged = false


func _ready():
	update_attribute_statistics()


func _process(_delta):
	# Update values every time a new statistics set is save in Statistics
	if Statistics.time != time_check:
		update_attribute_statistics()
		time_check = Statistics.time
	
	if is_mouse_dragged:
		set_global_position(get_global_mouse_position() - dragged_point)


func update_attribute_statistics():
	# Delet old values
	for child in back_panel.get_children():
		child.queue_free()

	title.text = Statistics.active_means_name
	var values = Statistics.active_means_array.slice(0,Statistics.time-1)

	# Limit the array length to 400
	if values.size() >= 400:
		var min_val = values.size() - 400
		values = values.slice(min_val, values.size())

	# Print all values saved for the selected attribute
	var counter = 0
	for value in values:
		if not value:
			continue
		
		if Statistics.active_means_name == "Total Fishes":
			mean_value_text.text = "Total number of fishes"
			value = 90 * value / values.max()
		
		# Mass goes from 0-1000 instead of 0-100
		if Statistics.active_means_name == "Mass":
			value = pow(value, 0.5)
		
		# Gender statistics are a special case done seperate
		if Statistics.active_means_name == "Gender":
			mean_value_text.text = "Female & Male"

			var female = float(value[0])/(value[0]+value[1])
			var male = float(value[1])/(value[0]+value[1])

			var rect_f = ColorRect.new()
			back_panel.add_child(rect_f)
			rect_f.rect_position = Vector2(2 + counter, 2)
			rect_f.rect_size = Vector2(1, 200*female)
			rect_f.color = Color("#B82563")
			
			var rect_m = ColorRect.new()
			back_panel.add_child(rect_m)
			rect_m.rect_position = Vector2(2 + counter, 2 + 200*female)
			rect_m.rect_size = Vector2(1, 200*male)
			rect_m.color = Color("#0088A8")

			counter += 1
			continue

		var rect = ColorRect.new()
		back_panel.add_child(rect)
		rect.rect_position = Vector2(2 + counter, 202-int(value*2))
		rect.rect_size = Vector2(1, int(value*2))
		rect.color = Color("#0088A8")

		counter += 1


func _on_Back_pressed():
	var menu_scene = load("res://UI/Statistics_main.tscn")
	var menu_instance = menu_scene.instance()
	get_parent().add_child(menu_instance)
	
	queue_free()


func _on_MarginContainer_mouse_entered():
	is_mouse_on_window = true


func _on_MarginContainer_mouse_exited():
	is_mouse_on_window = false
