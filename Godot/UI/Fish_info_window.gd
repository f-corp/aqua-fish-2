extends Control


var null_height = "-"
var null_width = "-"
var null_eye_size = "-"
var null_fin_size = "-"
var null_mouth_size = "-"
var null_teeth_size = "-"
var null_gender = "-"

var null_aggressive = "-"
var null_nervous = "-"
var null_hunger = "-"
var null_passion = "-"
var null_selfish = "-"
var null_conforming = "-"
var null_mutable = "-"

var selected_fish = null
var is_mouse_on_window = false
var is_mouse_dragged = false
var dragged_point = Vector2.ZERO


onready var height_info = $PanelOuter/MarginContainer/Panel/MarginContainer/VBoxContainer/HBoxContainer/External_values/Height
onready var width_info = $PanelOuter/MarginContainer/Panel/MarginContainer/VBoxContainer/HBoxContainer/External_values/Width
onready var eye_size_info = $PanelOuter/MarginContainer/Panel/MarginContainer/VBoxContainer/HBoxContainer/External_values/Eye_size
onready var fin_size_info = $PanelOuter/MarginContainer/Panel/MarginContainer/VBoxContainer/HBoxContainer/External_values/Fin_size
onready var mouth_size_info = $PanelOuter/MarginContainer/Panel/MarginContainer/VBoxContainer/HBoxContainer/External_values/Mouth_size
onready var teeth_size_info = $PanelOuter/MarginContainer/Panel/MarginContainer/VBoxContainer/HBoxContainer/External_values/Teeth_size
onready var gender_info = $PanelOuter/MarginContainer/Panel/MarginContainer/VBoxContainer/HBoxContainer/External_values/Gender

onready var aggressive_info = $PanelOuter/MarginContainer/Panel/MarginContainer/VBoxContainer/HBoxContainer/Internal_values/Aggressive
onready var nervous_info = $PanelOuter/MarginContainer/Panel/MarginContainer/VBoxContainer/HBoxContainer/Internal_values/Nervous
onready var hunger_info = $PanelOuter/MarginContainer/Panel/MarginContainer/VBoxContainer/HBoxContainer/Internal_values/Hunger
onready var passion_info = $PanelOuter/MarginContainer/Panel/MarginContainer/VBoxContainer/HBoxContainer/Internal_values/Passion
onready var selfish_info = $PanelOuter/MarginContainer/Panel/MarginContainer/VBoxContainer/HBoxContainer/Internal_values/Selfish
onready var conforming_info = $PanelOuter/MarginContainer/Panel/MarginContainer/VBoxContainer/HBoxContainer/Internal_values/Conforming
onready var mutable_info = $PanelOuter/MarginContainer/Panel/MarginContainer/VBoxContainer/HBoxContainer/Internal_values/Mutable


func _input(event):
	if event.is_action_pressed("left_click"):
		# A small delay
		$Timer.set_wait_time(0.1)
		$Timer.start()
	
	if event.is_action_pressed("left_click") and is_mouse_on_window:
		dragged_point = get_global_mouse_position() - rect_global_position
		is_mouse_dragged = true

	if event.is_action_released("left_click") and is_mouse_dragged:
		is_mouse_dragged = false


func _on_Timer_timeout():
	set_info_from_fish()


func _ready():
	Global.menu_fish_info_visable = true
	set_info_from_fish()


func _process(_delta):
	if is_mouse_dragged:
		set_global_position(get_global_mouse_position() - dragged_point)


func set_info_from_fish():
	selected_fish = get_selected_fish()
	
	if not selected_fish:
		set_null_info_values()
		return
	
	selected_fish.turn_on_arrow_info_selected()

	# the %.1f sets one decimal rounding
	height_info.text = str("%.1f" % selected_fish.height)
	width_info.text = str("%.1f" % selected_fish.width)
	eye_size_info.text = str("%.1f" % selected_fish.eye_size) 
	fin_size_info.text = str("%.1f" % selected_fish.fin_size) 
	mouth_size_info.text = str("%.1f" % selected_fish.mouth_size) 
	teeth_size_info.text = str("%.1f" % selected_fish.teeth_size) 
	gender_info.text = str(selected_fish.gender) 
	
	aggressive_info.text = str("%.1f" % selected_fish.aggressive)
	nervous_info.text = str("%.1f" % selected_fish.nervous)
	hunger_info.text = str("%.1f" % selected_fish.hunger)
	passion_info.text = str("%.1f" % selected_fish.passion)
	selfish_info.text = str("%.1f" % selected_fish.selfish)
	conforming_info.text = str("%.1f" % selected_fish.conforming)
	mutable_info.text = str("%.1f" % selected_fish.mutable)


func set_null_info_values():
	height_info.text = str(null_height)
	width_info.text = str(null_width)
	eye_size_info.text = str(null_eye_size) 
	fin_size_info.text = str(null_fin_size) 
	mouth_size_info.text = str(null_mouth_size) 
	teeth_size_info.text = str(null_teeth_size) 
	gender_info.text = str(null_gender) 
	
	aggressive_info.text = str(null_aggressive)
	nervous_info.text = str(null_nervous)
	hunger_info.text = str(null_hunger)
	passion_info.text = str(null_passion)
	selfish_info.text = str(null_selfish)
	conforming_info.text = str(null_conforming)
	mutable_info.text = str(null_mutable)


func get_selected_fish():
	var fishes_node = get_node("/root/Main/Fishes")
	for fish in fishes_node.get_children():
		if fish.is_info_selected:
			return fish


func back_to_ingame_menu():
	Global.menu_fish_info_visable = false

	var menu_scene = load("res://UI/Menu_base.tscn")
	var menu_instance = menu_scene.instance()
	get_parent().add_child(menu_instance)
	
	queue_free()


func clone_attributes_to_create_fish():
	var fish = get_selected_fish()

	if not fish:
		return

	CreateFishFromMenu.width = fish.width 
	CreateFishFromMenu.height = fish.height
	CreateFishFromMenu.mass = fish.mass
	CreateFishFromMenu.eyes = fish.eye_size
	CreateFishFromMenu.fin = fish.fin_size
	CreateFishFromMenu.mouth = fish.mouth_size
	CreateFishFromMenu.teeth = fish.teeth_size
	CreateFishFromMenu.aggressive = fish.aggressive
	CreateFishFromMenu.nervous = fish.nervous
	CreateFishFromMenu.hunger = fish.hunger
	CreateFishFromMenu.passion = fish.passion
	CreateFishFromMenu.selfish = fish.selfish
	CreateFishFromMenu.conforming = fish.conforming
	CreateFishFromMenu.mutable = fish.mutable

	fish.is_info_selected = false

	var create_fish_scene = load("res://UI/Create_fish.tscn")
	var create_fish_instance = create_fish_scene.instance()
	get_parent().add_child(create_fish_instance)
	queue_free()


func _on_MarginContainer_mouse_entered():
	is_mouse_on_window = true


func _on_MarginContainer_mouse_exited():
	is_mouse_on_window = false
	

func _on_InsideMarginContainer_mouse_entered():
	Global.menu_is_mouse_inside_info_menues = true


func _on_InsideMarginContainer_mouse_exited():
	Global.menu_is_mouse_inside_info_menues = false


func _on_Back_pressed():
	back_to_ingame_menu()


func _on_Clone_fish_pressed():
	clone_attributes_to_create_fish()
