extends Control


var is_mouse_on_window = false
var is_mouse_dragged = false
var dragged_point = Vector2.ZERO


func _input(event):
	if event.is_action_pressed("left_click") and is_mouse_on_window:
		dragged_point = get_global_mouse_position() - rect_global_position
		is_mouse_dragged = true

	if event.is_action_released("left_click") and is_mouse_dragged:
		is_mouse_dragged = false


func _ready():
	pass


func _process(_delta):
	if is_mouse_dragged:
		set_global_position(get_global_mouse_position() - dragged_point)


func back_to_main_menu():
	var _new_game = get_tree().change_scene("res://Main_menu/Main_menu.tscn")


func _on_Exit_to_main_menu_pressed():
	back_to_main_menu()


func _on_Back_to_game_pressed():
	Global.menu_visable = false
	queue_free()


func _on_MarginContainer_mouse_entered():
	is_mouse_on_window = true


func _on_MarginContainer_mouse_exited():
	is_mouse_on_window = false
	