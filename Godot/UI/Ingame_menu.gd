extends Control


var is_mouse_on_window = false
var is_mouse_dragged = false
var dragged_point = Vector2.ZERO


func _input(event):
	if event.is_action_pressed("left_click") and is_mouse_on_window:
		dragged_point = get_global_mouse_position() - rect_global_position
		is_mouse_dragged = true

	if event.is_action_released("left_click") and is_mouse_dragged:
		is_mouse_dragged = false


func _ready():
	pass # Replace with function body.


func _process(_delta):
	if is_mouse_dragged:
		set_global_position(get_global_mouse_position() - dragged_point)
	

func _on_MarginContainer_mouse_entered():
	is_mouse_on_window = true


func _on_MarginContainer_mouse_exited():
	is_mouse_on_window = false
