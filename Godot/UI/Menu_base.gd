extends Control

var screen_width = OS.get_screen_size().x
var screen_height = OS.get_screen_size().y

var is_mouse_on_window = false
var is_mouse_dragged = false
var dragged_point = Vector2.ZERO

func _input(event):
	if event.is_action_pressed("left_click") and is_mouse_on_window:
		dragged_point = get_global_mouse_position() - rect_global_position
		is_mouse_dragged = true

	if event.is_action_released("left_click") and is_mouse_dragged:
		is_mouse_dragged = false


func _ready():
	if OS.has_feature("HTML5"):
		$PanelOuter/MarginContainer/PanelInner/MarginContainer/VBoxContainer/Toggle_fullscreen.visible = false
	else:
		$PanelOuter/MarginContainer/PanelInner/MarginContainer/VBoxContainer/Toggle_fullscreen.visible = true


func _process(_delta):
	if is_mouse_dragged:
		set_global_position(get_global_mouse_position() - dragged_point)


func close_menu():
	Global.menu_visable = false
	queue_free()


func open_create_fish():
	var create_fish_scene = load("res://UI/Create_fish.tscn")
	var create_fish_instance = create_fish_scene.instance()
	get_parent().add_child(create_fish_instance)
	queue_free()


func open_fish_info():
	var fish_info_scene = load("res://UI/Fish_info_window.tscn")
	var fish_info_instance = fish_info_scene.instance()
	fish_info_instance.rect_position.x = -int(screen_width*0.5) + 20
	fish_info_instance.rect_position.y = int(screen_height*0.5) - 174 - 20
	get_parent().add_child(fish_info_instance)
	queue_free()
	

func open_statistics():
	var create_fish_scene = load("res://UI/Statistics_main.tscn")
	var create_fish_instance = create_fish_scene.instance()
	get_parent().add_child(create_fish_instance)
	queue_free()


func open_back_to_menu_warning():
	var back_to_menu_warning = load("res://UI/Back_to_main_warning.tscn").instance()
	get_parent().add_child(back_to_menu_warning)
	queue_free()


func _on_Close_pressed():
	close_menu()


func _on_Create_fish_pressed():
	open_create_fish()


func _on_Fish_info_pressed():
	open_fish_info()


func _on_Statistics_pressed():
	open_statistics()


func _on_Drop_carrot_pressed():
	get_tree().get_root().get_node("Main").add_carrot()


func _on_Toggle_bars_pressed():
	var fishes = get_tree().get_root().get_node("Main").get_node("Fishes").get_children()
	for fish in fishes:
		fish.toggel_health_energy_bars()


func _on_Toggle_fullscreen_pressed():
	OS.window_fullscreen = !OS.window_fullscreen


func _on_Back_to_main_menu_pressed():
	open_back_to_menu_warning()


func _on_MarginContainer_mouse_entered():
	is_mouse_on_window = true


func _on_MarginContainer_mouse_exited():
	is_mouse_on_window = false

