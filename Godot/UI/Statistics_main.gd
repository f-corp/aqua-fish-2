extends Control


# All label nodes
onready var total_fishes_label = $PanelOuter/MarginContainer/PanelInner/MarginContainer/ScrollContainer/VBoxContainer/Total_amount/Label
onready var height_label = $PanelOuter/MarginContainer/PanelInner/MarginContainer/ScrollContainer/VBoxContainer/Height/Label
onready var width_label = $PanelOuter/MarginContainer/PanelInner/MarginContainer/ScrollContainer/VBoxContainer/Width/Label
onready var mass_label = $PanelOuter/MarginContainer/PanelInner/MarginContainer/ScrollContainer/VBoxContainer/Mass/Label
onready var fin_size_label = $PanelOuter/MarginContainer/PanelInner/MarginContainer/ScrollContainer/VBoxContainer/Fin_size/Label
onready var eye_size_label = $PanelOuter/MarginContainer/PanelInner/MarginContainer/ScrollContainer/VBoxContainer/Eye_size/Label
onready var mouth_size_label = $PanelOuter/MarginContainer/PanelInner/MarginContainer/ScrollContainer/VBoxContainer/Mouth_size/Label
onready var teeth_size_label = $PanelOuter/MarginContainer/PanelInner/MarginContainer/ScrollContainer/VBoxContainer/Teeth_size/Label
onready var gender_label = $PanelOuter/MarginContainer/PanelInner/MarginContainer/ScrollContainer/VBoxContainer/Gender/Label
onready var aggressive_label = $PanelOuter/MarginContainer/PanelInner/MarginContainer/ScrollContainer/VBoxContainer/Aggressive/Label
onready var nervous_label = $PanelOuter/MarginContainer/PanelInner/MarginContainer/ScrollContainer/VBoxContainer/Nervous/Label
onready var hunger_label = $PanelOuter/MarginContainer/PanelInner/MarginContainer/ScrollContainer/VBoxContainer/Hunger/Label
onready var passion_label = $PanelOuter/MarginContainer/PanelInner/MarginContainer/ScrollContainer/VBoxContainer/Passion/Label
onready var selfish_label = $PanelOuter/MarginContainer/PanelInner/MarginContainer/ScrollContainer/VBoxContainer/Selfish/Label
onready var conforming_label = $PanelOuter/MarginContainer/PanelInner/MarginContainer/ScrollContainer/VBoxContainer/Conforming/Label
onready var mutable_label = $PanelOuter/MarginContainer/PanelInner/MarginContainer/ScrollContainer/VBoxContainer/Mutable/Label

onready var header_label = $PanelOuter/MarginContainer/PanelInner/MarginContainer/ScrollContainer/VBoxContainer/Head/Header

var time_check = 0

var is_mouse_on_window = false
var is_mouse_dragged = false
var dragged_point = Vector2.ZERO


func _input(event):
	if event.is_action_pressed("left_click") and is_mouse_on_window:
		dragged_point = get_global_mouse_position() - rect_global_position
		is_mouse_dragged = true

	if event.is_action_released("left_click") and is_mouse_dragged:
		is_mouse_dragged = false


func _process(_delta):
	# Update values every time a new statistics set is save in Statistics
	if Statistics.time != time_check:
		update_mean_values()
		time_check = Statistics.time
	
	if is_mouse_dragged:
		set_global_position(get_global_mouse_position() - dragged_point)


func update_mean_values():
	if not Global.statistics_logging_on:
		header_label.text = "Statistics are turned off"
		return
	
	total_fishes_label.text = str(Statistics.total_fishes[Statistics.time-1]) + " fishes"
	height_label.text = str("%.1f" % Statistics.height_means[Statistics.time-1])
	width_label.text = str("%.1f" % Statistics.width_means[Statistics.time-1])
	mass_label.text = str("%.1f" % Statistics.mass_means[Statistics.time-1])
	fin_size_label.text = str("%.1f" % Statistics.fin_size_means[Statistics.time-1])
	eye_size_label.text = str("%.1f" % Statistics.eye_size_means[Statistics.time-1])
	mouth_size_label.text = str("%.1f" % Statistics.mouth_size_means[Statistics.time-1])
	teeth_size_label.text = str("%.1f" % Statistics.teeth_size_means[Statistics.time-1])

	gender_label.text = "f: " + str(Statistics.gender_sums[Statistics.time-1][0]) +\
						"  m: " + str(Statistics.gender_sums[Statistics.time-1][1])
	
	aggressive_label.text = str("%.1f" % Statistics.aggressive_means[Statistics.time-1])
	nervous_label.text = str("%.1f" % Statistics.nervous_means[Statistics.time-1])
	hunger_label.text = str("%.1f" % Statistics.hunger_means[Statistics.time-1])
	passion_label.text = str("%.1f" % Statistics.passion_means[Statistics.time-1])
	selfish_label.text = str("%.1f" % Statistics.selfish_means[Statistics.time-1])
	conforming_label.text = str("%.1f" % Statistics.conforming_means[Statistics.time-1])
	mutable_label.text = str("%.1f" % Statistics.mutable_means[Statistics.time-1])



func open_statistics_attribute():
	var menu_scene = load("res://UI/Statistics_attribute.tscn")
	var menu_instance = menu_scene.instance()
	Global.menu_is_mouse_inside_menues = false
	get_parent().add_child(menu_instance)
	
	queue_free()


func _on_Button_back_pressed():
	var menu_scene = load("res://UI/Menu_base.tscn")
	var menu_instance = menu_scene.instance()
	Global.menu_is_mouse_inside_menues = false
	get_parent().add_child(menu_instance)
	
	queue_free()


func _on_Button_total_fishes_pressed():
	Statistics.active_means_name = "Total Fishes"
	Statistics.active_means_array = Statistics.total_fishes
	open_statistics_attribute()


func _on_Button_height_pressed():
	Statistics.active_means_name = "Height"
	Statistics.active_means_array = Statistics.height_means
	open_statistics_attribute()


func _on_Button_width_pressed():
	Statistics.active_means_name = "Width"
	Statistics.active_means_array = Statistics.width_means
	open_statistics_attribute()


func _on_Button_mass_pressed():
	Statistics.active_means_name = "Mass"
	Statistics.active_means_array = Statistics.mass_means
	open_statistics_attribute()


func _on_Button_fin_size_pressed():
	Statistics.active_means_name = "Fin Size"
	Statistics.active_means_array = Statistics.fin_size_means
	open_statistics_attribute()


func _on_Button_eye_size_pressed():
	Statistics.active_means_name = "Eye Size"
	Statistics.active_means_array = Statistics.eye_size_means
	open_statistics_attribute()


func _on_Button_mouth_size_pressed():
	Statistics.active_means_name = "Mouth Size"
	Statistics.active_means_array = Statistics.mouth_size_means
	open_statistics_attribute()


func _on_Button_teeth_size_pressed():
	Statistics.active_means_name = "Teeth Size"
	Statistics.active_means_array = Statistics.teeth_size_means
	open_statistics_attribute()


func _on_Button_gender_pressed():
	Statistics.active_means_name = "Gender"
	Statistics.active_means_array = Statistics.gender_sums
	open_statistics_attribute()


func _on_Button_aggressive_pressed():
	Statistics.active_means_name = "Aggressive"
	Statistics.active_means_array = Statistics.aggressive_means
	open_statistics_attribute()


func _on_Button_nervous_pressed():
	Statistics.active_means_name = "Nervous"
	Statistics.active_means_array = Statistics.nervous_means
	open_statistics_attribute()


func _on_Button_hunger_pressed():
	Statistics.active_means_name = "Hunger"
	Statistics.active_means_array = Statistics.hunger_means
	open_statistics_attribute()


func _on_Button_passion_pressed():
	Statistics.active_means_name = "Passion"
	Statistics.active_means_array = Statistics.passion_means
	open_statistics_attribute()


func _on_Button_selfish_pressed():
	Statistics.active_means_name = "Selfish"
	Statistics.active_means_array = Statistics.selfish_means
	open_statistics_attribute()


func _on_Button_conforming_pressed():
	Statistics.active_means_name = "Conforming"
	Statistics.active_means_array = Statistics.conforming_means
	open_statistics_attribute()


func _on_Button_mutable_pressed():
	Statistics.active_means_name = "Mutable"
	Statistics.active_means_array = Statistics.mutable_means
	open_statistics_attribute()


func _on_MarginContainer_mouse_entered():
	is_mouse_on_window = true
	Global.menu_is_mouse_inside_menues = true

func _on_MarginContainer_mouse_exited():
	is_mouse_on_window = false
	Global.menu_is_mouse_inside_menues = false


func _on_MarginContainer_inside_mouse_entered():
	Global.menu_is_mouse_inside_menues = true
	
func _on_MarginContainer_inside_mouse_exited():
	Global.menu_is_mouse_inside_menues = false


func _on_ScrollContainer_inner_mouse_entered():
	Global.menu_is_mouse_inside_menues = true
	
func _on_ScrollContainer_inner_mouse_exited():
	Global.menu_is_mouse_inside_menues = false
