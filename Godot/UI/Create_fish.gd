extends Control

# Store all sliders in a horible probably inefficient way
onready var width_slider = $PanelOuter/MarginContainer/PanelInner/MarginContainer/VBoxContainer/VBoxContainer/Width/width_slider
onready var height_slider = $PanelOuter/MarginContainer/PanelInner/MarginContainer/VBoxContainer/VBoxContainer/Height/height_slider
onready var mass_slider = $PanelOuter/MarginContainer/PanelInner/MarginContainer/VBoxContainer/VBoxContainer/Mass/mass_slider
onready var eyes_slider = $PanelOuter/MarginContainer/PanelInner/MarginContainer/VBoxContainer/VBoxContainer/Eyes/eyes_slider
onready var fin_slider = $PanelOuter/MarginContainer/PanelInner/MarginContainer/VBoxContainer/VBoxContainer/Fin/fin_slider
onready var mouth_slider = $PanelOuter/MarginContainer/PanelInner/MarginContainer/VBoxContainer/VBoxContainer/Mouth/mouth_slider
onready var teeth_slider = $PanelOuter/MarginContainer/PanelInner/MarginContainer/VBoxContainer/VBoxContainer/Teeth/teeth_slider
onready var aggressive_slider = $PanelOuter/MarginContainer/PanelInner/MarginContainer/VBoxContainer/VBoxContainer/Aggressive/aggressive_slider
onready var nervous_slider = $PanelOuter/MarginContainer/PanelInner/MarginContainer/VBoxContainer/VBoxContainer/Nervous/nervous_slider
onready var hunger_slider = $PanelOuter/MarginContainer/PanelInner/MarginContainer/VBoxContainer/VBoxContainer/Hunger/hunger_slider
onready var passion_slider = $PanelOuter/MarginContainer/PanelInner/MarginContainer/VBoxContainer/VBoxContainer/Passion/passion_slider
onready var selfish_slider = $PanelOuter/MarginContainer/PanelInner/MarginContainer/VBoxContainer/VBoxContainer/Selfish/selfish_slider
onready var conforming_slider = $PanelOuter/MarginContainer/PanelInner/MarginContainer/VBoxContainer/VBoxContainer/Conforming/conforming_slider
onready var mutable_slider = $PanelOuter/MarginContainer/PanelInner/MarginContainer/VBoxContainer/VBoxContainer/Mutable/mutable_slider

var rng = RandomNumberGenerator.new()

var is_mouse_on_window = false
var is_mouse_dragged = false
var dragged_point = Vector2.ZERO


func _input(event):
	if event.is_action_pressed("left_click") and is_mouse_on_window:
		dragged_point = get_global_mouse_position() - rect_global_position
		is_mouse_dragged = true

	if event.is_action_released("left_click") and is_mouse_dragged:
		is_mouse_dragged = false


func _ready():
	stage_a_fish()


func _process(_delta):
	if is_mouse_dragged:
		set_global_position(get_global_mouse_position() - dragged_point)
	

func create_the_fish():
	var fishes_node = get_node("/root/Main/Fishes")
	for fish in fishes_node.get_children():
		if fish.is_menu_created:
			fish.is_active = true
			fish.is_menu_created = false
			fish.vel.x = -10*rng.randf()
			fish.vel.y = -10*(1-2*randf())
			fish.not_colliding_timer_sec(1)
			fish.update_size_and_animations()
	stage_a_fish()
	


func stage_a_fish():
	var fishes_node = get_node("/root/Main/Fishes")
	
	var fish_scene = load("res://fish/Fish.tscn")
	var fish_instance = fish_scene.instance()
	
	fish_instance.is_active = false
	fish_instance.is_menu_created = true
	
	fishes_node.add_child(fish_instance)
	fish_instance.get_attributes_from_create_menu()
	fish_instance.update_size()
	set_sliders_as_stored_values()
	update_created_fish()


func back_to_ingame_menu():
	var menu_scene = load("res://UI/Menu_base.tscn")
	var menu_instance = menu_scene.instance()
	get_parent().add_child(menu_instance)
	
	# Delete creaded fish
	var fishes_node = get_node("/root/Main/Fishes")
	for fish in fishes_node.get_children():
		if fish.is_menu_created:
			fish.queue_free()
	# Delete menu
	queue_free()


func update_created_fish():
	var fishes_node = get_node("/root/Main/Fishes")
	for fish in fishes_node.get_children():
		if fish.is_menu_created:
			fish.get_attributes_from_create_menu()
			fish.update_size_and_animations()


func set_sliders_as_stored_values():
	width_slider.value = CreateFishFromMenu.width
	height_slider.value = CreateFishFromMenu.height
	mass_slider.value = CreateFishFromMenu.mass
	eyes_slider.value = CreateFishFromMenu.eyes
	fin_slider.value = CreateFishFromMenu.fin
	mouth_slider.value = CreateFishFromMenu.mouth
	teeth_slider.value = CreateFishFromMenu.teeth
	aggressive_slider.value = CreateFishFromMenu.aggressive
	nervous_slider.value = CreateFishFromMenu.nervous
	hunger_slider.value = CreateFishFromMenu.hunger
	passion_slider.value = CreateFishFromMenu.passion
	selfish_slider.value = CreateFishFromMenu.selfish
	conforming_slider.value = CreateFishFromMenu.conforming
	mutable_slider.value = CreateFishFromMenu.mutable


func _on_Create_fish_pressed():
	create_the_fish()	

func _on_Back_pressed():
	back_to_ingame_menu()

	
# Take input from sliders
func _on_height_slider_value_changed(height):
	CreateFishFromMenu.height = height
	CreateFishFromMenu.mass = CreateFishFromMenu.width * height
	mass_slider.value = CreateFishFromMenu.mass
	update_created_fish()

func _on_width_slider_value_changed(width):
	CreateFishFromMenu.width = width
	CreateFishFromMenu.mass = CreateFishFromMenu.height * width
	mass_slider.value = CreateFishFromMenu.mass
	update_created_fish()

func _on_eyes_slider_value_changed(eyes):
	CreateFishFromMenu.eyes = eyes
	update_created_fish()

func _on_fin_slider_value_changed(fin):
	CreateFishFromMenu.fin = fin
	update_created_fish()

func _on_mouth_slider_value_changed(mouth):
	CreateFishFromMenu.mouth = mouth
	update_created_fish()

func _on_teeth_slider_value_changed(teeth):
	CreateFishFromMenu.teeth = teeth
	update_created_fish()

func _on_aggressive_slider_value_changed(aggressive):
	CreateFishFromMenu.aggressive = aggressive
	update_created_fish()

func _on_nervous_slider_value_changed(nervous):
	CreateFishFromMenu.nervous = nervous
	update_created_fish()

func _on_hunger_slider_value_changed(hunger):
	CreateFishFromMenu.hunger = hunger
	update_created_fish()

func _on_passion_slider_value_changed(passion):
	CreateFishFromMenu.passion = passion
	update_created_fish()

func _on_selfish_slider_value_changed(selfish):
	CreateFishFromMenu.selfish = selfish
	update_created_fish()

func _on_conforming_slider_value_changed(conforming):
	CreateFishFromMenu.conforming = conforming
	update_created_fish()

func _on_mutable_slider_value_changed(mutable):
	CreateFishFromMenu.mutable = mutable
	update_created_fish()

func _on_Reset_pressed():
	CreateFishFromMenu.width = 10
	CreateFishFromMenu.height = 10
	CreateFishFromMenu.mass = 100
	CreateFishFromMenu.eyes = 10
	CreateFishFromMenu.fin = 10
	CreateFishFromMenu.mouth = 10
	CreateFishFromMenu.teeth = 10
	CreateFishFromMenu.aggressive = 10
	CreateFishFromMenu.nervous = 10
	CreateFishFromMenu.hunger = 10
	CreateFishFromMenu.passion = 10
	CreateFishFromMenu.selfish = 10
	CreateFishFromMenu.conforming = 10
	CreateFishFromMenu.mutable = 10

	set_sliders_as_stored_values()
	update_created_fish()
	

func _on_MarginContainer_mouse_entered():
	is_mouse_on_window = true


func _on_MarginContainer_mouse_exited():
	is_mouse_on_window = false
	