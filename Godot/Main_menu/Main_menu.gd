extends Node2D


var world_width = OS.get_screen_size().x
var world_height = OS.get_screen_size().y


var rng = RandomNumberGenerator.new()


func _ready():
	$VersionNumber.text = "v" + Global.version
	$VersionNumber.rect_position.x = world_width*0.85
	$VersionNumber.rect_position.y = world_height*0.85

	Engine.time_scale = 1
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

	OS.window_fullscreen = true
	OS.set_window_size(Vector2(world_width, world_height))
	$Background.rect_size = Vector2(world_width, world_height)
	$WhiteBlur.rect_scale = Vector2(world_width/1920, world_height/1080)
	$Camera2D.position = Vector2(world_width, world_height)/2
	
	rng.randomize()
	add_fishes_to_background()
	add_bubbles_to_background()

	place_main_menu_text()
	place_options_text()
	$Main_menu_text.modulate = Color(1,1,1,1)
	$Background/ColorRect.color = Color(1,1,1,1)
	$AnimationPlayerToWhite.play("WhiteBlurInv")

	play_and_set_ambient_bubble()


func _process(_delta):
	pass


func main_menu_new_game():
	$AnimationPlayerToWhite.play("WhiteBlur")

func main_menu_credits():
	$AnimationPlayerToWhite.play("BlurCredits")

func _on_AnimationPlayerToWhite_animation_finished(anim):
	if anim == "WhiteBlur":
		var _new_game = get_tree().change_scene("res://Main.tscn")
	if anim == "BlurCredits":
		var _credits = get_tree().change_scene("res://Main_menu/Credits.tscn")


func main_menu_options():
	$AnimationPlayerTextFade.play("main_menu_out")

func options_back_to_main():
	$AnimationPlayerTextFade.play("options_out")

func _on_AnimationPlayerTextFade_animation_finished(anim):
	if anim == "main_menu_out":
		$Main_menu_text.visible = false
		$Options_text.visible = true
		$AnimationPlayerTextFade.play("options_in")
	if anim == "options_out":
		$Options_text.visible = false
		$Main_menu_text.visible = true
		$AnimationPlayerTextFade.play("main_menu_in")


func main_menu_exit_game():
	if OS.has_feature('JavaScript'):
		JavaScript.eval("window.location.href='https://en.wikipedia.org/wiki/Fish'")
	else:
		get_tree().quit()


func add_fishes_to_background():
	for _i in Global.main_menu_fish_amount:
		var fish_instance = load("res://fish/Fish_menu.tscn").instance()

		fish_instance.position.x = rng.randf_range(50, world_width-50)
		fish_instance.position.y = rng.randf_range(50, world_height-50)

		$Fishes.add_child(fish_instance)


func add_bubbles_to_background():
	var bubble_instance = load("res://Main_menu/Bubble_menu.tscn").instance()
	bubble_instance.position.x = world_width*0.12
	bubble_instance.position.y = world_height
	$Bubbles.add_child(bubble_instance)

	bubble_instance = load("res://Main_menu/Bubble_menu.tscn").instance()
	bubble_instance.position.x = world_width*0.88
	bubble_instance.position.y = world_height
	$Bubbles.add_child(bubble_instance)


func place_main_menu_text():
	# The text is placed using a 864 high world and scaled from that
	$Main_menu_text.rect_scale.x = world_height/864
	$Main_menu_text.rect_scale.y = world_height/864

	$Main_menu_text.rect_position.x = int(world_width*0.5 + 500*(world_height/864))
	$Main_menu_text.rect_position.y = int(world_height*0.15)


func place_options_text():
	$Options_text.rect_scale.x = world_height/864
	$Options_text.rect_scale.y = world_height/864

	$Options_text.rect_position.x = int(60 *(world_width/864))
	$Options_text.rect_position.y = int(15 *(world_width/864))


func play_and_set_ambient_bubble():
	$Timer_ambient_bubble.wait_time = rng.randf_range(10,20)
	$Timer_ambient_bubble.start()
	$AudioStreamPlayer_ambient_bubble.play()

func _on_Timer_ambient_bubble_timeout():
	play_and_set_ambient_bubble()
