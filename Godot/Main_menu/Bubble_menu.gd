extends Particles2D

var rng = RandomNumberGenerator.new()

func _ready():
	rng.randomize()
	randomness = rng.randf_range(0.8, 1)
	explosiveness = rng.randf_range(0.05, 0.2)
