extends Control


var world_width = OS.get_screen_size().x
var world_height = OS.get_screen_size().y


export var text_crawl_speed = 0.18

var credits_text = CreditsText.credits_text

func _input(event):
	if event.is_action_released("open_menu") and not $AnimationPlayerFade.is_playing():
		fade_to_main_menu()


func _ready():
	Engine.time_scale = 1
	OS.window_fullscreen = true
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)

	$Background.rect_scale = Vector2(world_width/1920, world_height/1080)
	$Background/ColorRect.rect_scale = Vector2(world_width/1920, world_height/1080)
	$BlackRect.rect_scale = Vector2(world_width/1920, world_height/1080)
	
	$Text.rect_position.y = OS.get_screen_size().y

	for text in credits_text:
		var text_instance = load("res://Main_menu/credit_text_small.tscn").instance()
		if text[1] == "b":
			text_instance = load("res://Main_menu/credit_text_big.tscn").instance()
		elif text[1] == "si":
			text_instance = load("res://Main_menu/credit_text_small_i.tscn").instance()
		text_instance.text = text[0]
		$Text.add_child(text_instance)
	
	# Fade in
	$BlackRect.color = Color(0,0,0,1)
	$AnimationPlayerFade.play("FadeIn")
	$MusicEffects.play("MusicAnimation")


func _process(delta):
	var delta_normalization = delta / 0.006944
	for row in $Text.get_children():
		row.rect_position.y -= text_crawl_speed * delta_normalization


func back_to_main_menu():
	var _scene = get_tree().change_scene("res://Main_menu/Main_menu.tscn")


func fade_to_main_menu():
	$BlackRect.visible = true
	$AnimationPlayerFade.play("FadeOut")
	
func _on_AnimationPlayerFade_animation_finished(anim):
	if anim == "FadeIn":
		$BlackRect.visible = false
	if anim == "FadeOut":
		back_to_main_menu()		


func _on_AudioStreamPlayer_finished():
	back_to_main_menu()
