extends Control


var base_world_size = Global.world_size_screen_multiple
var base_carrot_drop_rate = Global.carrot_drop_timer_max
var base_simulation_speed = Global.simulation_time_scale
var base_statistics_on = Global.statistics_logging_on
var base_statistics_time = Global.statistics_time_intervals
var base_fish_start_amount = Global.start_fish_amount
var base_fish_attribute_min = Global.fish_attribute_start_value_min
var base_fish_attribute_max = Global.fish_attribute_start_value_max
var base_sound_is_off = Global.sound_is_off
var base_sound_master_volume = Global.sound_master_volume


onready var world_size_slider = $VBoxContainer/HBoxContainer/FirstColumn/World_size/Slider/HSlider
onready var carrot_drop_slider = $VBoxContainer/HBoxContainer/FirstColumn/Carrot_drop_rate/Slider/HSlider
onready var simulation_time_slider = $VBoxContainer/HBoxContainer/FirstColumn/Simulation_speed/Slider/HSlider
onready var statistics_on_off_button = $VBoxContainer/HBoxContainer/FirstColumn/Statistics/OnOff/CheckButton
onready var statistics_time_input = $VBoxContainer/HBoxContainer/FirstColumn/Statistics/Time/Time_input

onready var fish_start_amount_input = $VBoxContainer/HBoxContainer/SecondColumn/Fish_star_amount/Slider/Fish_amount
onready var fish_attribute_min_slider = $VBoxContainer/HBoxContainer/SecondColumn/Fish_attributes/Slider_low/HSlider
onready var fish_attribute_min_input = $VBoxContainer/HBoxContainer/SecondColumn/Fish_attributes/Slider_low/Number
onready var fish_attribute_max_slider = $VBoxContainer/HBoxContainer/SecondColumn/Fish_attributes/Slider_high/HSlider
onready var fish_attribute_max_input = $VBoxContainer/HBoxContainer/SecondColumn/Fish_attributes/Slider_high/Number

onready var sounds_on_off_button = $VBoxContainer/HBoxContainer/SecondColumn/Sound/OnOff2/CheckButton
onready var sounds_volume_slider = $VBoxContainer/HBoxContainer/SecondColumn/Sound/Slider_high/HSlider
onready var sounds_volume_input = $VBoxContainer/HBoxContainer/SecondColumn/Sound/Slider_high/Number


func _ready():
	modulate = Color(1,1,1,0)
	set_slider_start_values()


func set_slider_start_values():
	world_size_slider.value = base_world_size
	carrot_drop_slider.value = base_carrot_drop_rate
	simulation_time_slider.value = base_simulation_speed
	statistics_on_off_button.pressed = base_statistics_on
	statistics_time_input.text = str(base_statistics_time)
	fish_start_amount_input.text = str(base_fish_start_amount)
	fish_attribute_min_slider.value = base_fish_attribute_min
	fish_attribute_min_input.text = str(base_fish_attribute_min)
	fish_attribute_max_slider.value = base_fish_attribute_max
	fish_attribute_max_input.text = str(base_fish_attribute_max)
	sounds_on_off_button.pressed = !base_sound_is_off
	sounds_volume_slider.value = base_sound_master_volume
	sounds_volume_input.text = str(base_sound_master_volume)


func reset_initial_values():
	world_size_slider.value = GlobalOptionsReset.base_world_size
	Global.world_size_screen_multiple = GlobalOptionsReset.base_world_size

	carrot_drop_slider.value = GlobalOptionsReset.base_carrot_drop_rate
	Global.carrot_drop_timer_max = GlobalOptionsReset.base_carrot_drop_rate

	simulation_time_slider.value = GlobalOptionsReset.base_simulation_speed
	Global.simulation_time_scale = GlobalOptionsReset.base_simulation_speed

	statistics_on_off_button.pressed = GlobalOptionsReset.base_statistics_on
	Global.statistics_logging_on = GlobalOptionsReset.base_statistics_on

	statistics_time_input.text = str(GlobalOptionsReset.base_statistics_time)
	Global.statistics_time_intervals = GlobalOptionsReset.base_statistics_time

	fish_start_amount_input.text = str(GlobalOptionsReset.base_fish_start_amount)
	Global.start_fish_amount = GlobalOptionsReset.base_fish_start_amount

	fish_attribute_min_slider.value = GlobalOptionsReset.base_fish_attribute_min
	fish_attribute_min_input.text = str(GlobalOptionsReset.base_fish_attribute_min)
	Global.fish_attribute_start_value_min = GlobalOptionsReset.base_fish_attribute_min
	
	fish_attribute_max_slider.value = GlobalOptionsReset.base_fish_attribute_max
	fish_attribute_max_input.text = str(GlobalOptionsReset.base_fish_attribute_max)
	Global.fish_attribute_start_value_max = GlobalOptionsReset.base_fish_attribute_max

	sounds_on_off_button.pressed = !GlobalOptionsReset.base_sound_is_off
	Global.sound_is_off = GlobalOptionsReset.base_sound_is_off
	AudioServer.set_bus_mute(AudioServer.get_bus_index("Master"), GlobalOptionsReset.base_sound_is_off)

	sounds_volume_slider.value = GlobalOptionsReset.base_sound_master_volume
	sounds_volume_input.text = str(GlobalOptionsReset.base_sound_master_volume)
	Global.sound_master_volume = GlobalOptionsReset.base_sound_master_volume
	set_master_volume(GlobalOptionsReset.base_sound_master_volume)


func _on_HSlider_world_size_value_changed(value):
	Global.world_size_screen_multiple = value


func _on_HSlider_carrot_drop_rate_value_changed(value):
	Global.carrot_drop_timer_max = value


func _on_HSlider_simulation_speed_value_changed(value):
	Global.simulation_time_scale = value


func _on_CheckButton_statistics_on_off_toggled(button_pressed):
	Global.statistics_logging_on = button_pressed

	
func _on_Time_input_statistics_time_text_entered(new_text):
	if not new_text.is_valid_integer():
		statistics_time_input.text = str(10)
		return
	
	if int(new_text) == 0:
		statistics_time_input.text = str(Global.statistics_time_intervals)
		return
	
	Global.statistics_time_intervals = int(new_text)


func _on_LineEdit_fish_amount_text_entered(new_text):
	if not new_text.is_valid_integer():
		fish_start_amount_input.text = str(Global.start_fish_amount)
		return
	
	Global.start_fish_amount = int(new_text)


""" Attribute min value set """
func _on_HSlider_attribute_low_value_changed(value):
	set_attribute_min_value(value)

func _on_Number_attribute_low_text_entered(new_text):
	if not new_text.is_valid_integer():
		fish_attribute_min_input.text = str(Global.fish_attribute_start_value_min)
		return
	set_attribute_min_value(int(new_text))

func set_attribute_min_value(value):
	# make sure start attribute is biger than 0
	while value - Global.fish_attribute_start_variance <= 1:
		value += 1
	
	fish_attribute_min_slider.value = value
	fish_attribute_min_input.text = str(value)
	Global.fish_attribute_start_value_min = value

	if Global.fish_attribute_start_value_min >= Global.fish_attribute_start_value_max:
		fish_attribute_max_slider.value = value
		fish_attribute_max_input.text = str(value)
		Global.fish_attribute_start_value_max = value


""" Attribute max value set """
func _on_HSlider_attribute_max_value_changed(value):
	set_attribute_max_value(value)

func _on_Number_attribute_max_text_entered(new_text):
	if not new_text.is_valid_integer():
		fish_attribute_max_input.text = str(Global.fish_attribute_start_value_max)
		return
	set_attribute_max_value(int(new_text))

func set_attribute_max_value(value):
	while value - Global.fish_attribute_start_variance <= 1:
		value += 1
	
	# also make sure max is bigger than min
	while value < Global.fish_attribute_start_value_min:
		value += 1
	
	fish_attribute_max_slider.value = value
	fish_attribute_max_input.text = str(value)
	Global.fish_attribute_start_value_max = value


""" Sound stuff """
func _on_CheckButton_sound_on_off_toggled(button_pressed):
	Global.sound_is_off = !button_pressed
	AudioServer.set_bus_mute(AudioServer.get_bus_index("Master"), !button_pressed)
	
func _on_HSlider_sound_master_volume_value_changed(value):
	Global.sound_master_volume = value
	set_master_volume(value)

func _on_Number_sound_master_volume_text_entered(new_text):
	if not new_text.is_valid_integer():
		sounds_volume_input.text = str(Global.sound_master_volume)
		return
	set_master_volume(int(new_text))

func set_master_volume(value):
	var volume = 0.2*value - 14 # matte 2

	sounds_volume_slider.value = value
	sounds_volume_input.text = str(value)
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), volume)

""""""

func _on_Button_reset_pressed():
	reset_initial_values()


func _on_Button_back_to_main_pressed():
	get_parent().options_back_to_main()

