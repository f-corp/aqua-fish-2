extends Control

""" Just send the button pressed information to main menu script """

func _on_New_game_pressed():
	get_parent().main_menu_new_game()


func _on_Options_pressed():
	get_parent().main_menu_options()


func _on_Credits_pressed():
	get_parent().main_menu_credits()


func _on_Exit_game_pressed():
	get_parent().main_menu_exit_game()

