extends KinematicBody2D


# Movement
var force_from_fins = Vector2(0,0)

var force = Vector2(0,0)
var acc = Vector2(0,0)
var vel = Vector2(0,0)

var vel_previous = Vector2(0,0)
var pos_previous = Vector2(0,0)
var idle_force_direction = Vector2(0,0)

# Fish states
var behaviour = "do nothing"
var needs_new_behaviour = true

var is_alive = true
var is_eating = false
var is_looking_at_food = false
var is_facing_left = true
var is_idle_swimming = false
var is_pregnant = false
var is_a_fish_child = false
var is_hunting = false
var is_attacked = true
var is_kissing = false

var is_active = true
var is_menu_created = false
var is_info_selected = false
var is_mouse_dragged = false
var is_mouse_on_fish = false

var chosen_partner = null
var chosen_prey = null
var attacking_fish = null
var father_of_child = null

# Fish Attributes
var height = 10
var width = 10
var mass = 100
var mass_movement = 36.0
var eye_size = 10
var fin_size = 10
var mouth_size = 10
var teeth_size = 10
var gender: String = "female"
var aggressive = 10
var nervous = 10
var hunger = 10
var passion = 10
var selfish = 10
var conforming = 10
var mutable = 10

# Fish Outputs
var fin_force
var water_resistance
var health
var health_max
var energy
var energy_max
var energy_drain
var eat_speed
var food_energy_value
var food_energy_value_max
var food_capability = []
var awareness
var attack
var defence

# Object lists
var detected_carrots = []
var detected_berries = []
var detected_fishes = []

# Fish inner workings
var heal_per_second_percent = Global.fish_heal_per_second_percent
var energy_drain_multiplyer = Global.fish_energy_drain_multiplyer

# Misc
var rng = RandomNumberGenerator.new()
var last_delta = 0
var dragged_point = Vector2.ZERO
var is_in_main_menu = false

var world_width = OS.get_screen_size().x*Global.world_size_screen_multiple
var world_height = OS.get_screen_size().y*Global.world_size_screen_multiple
var abs_bottom_height = world_height*(1-Global.world_floor_absolute_bottom)

onready var health_bar = $Sliders/HealthBar
onready var energy_bar = $Sliders/EnergyBar

onready var info_selected_arrow = $info_selected_arrow


func _input(event):
	""" All inputs """
	if event.is_action_pressed("left_click"):
		# Reset all fishes so only one is selected to display info
		if not Global.menu_is_mouse_inside_info_menues:
			is_info_selected = false
			info_selected_arrow.visible = false

	if event.is_action_pressed("left_click") and is_mouse_on_fish:
		is_mouse_dragged = true
		is_info_selected = true
		dragged_point = get_global_mouse_position() - global_position

		# Pick only one fish
		for fish in get_parent().get_children():
			if fish.get_index() != get_index() and fish.is_mouse_dragged:
				is_mouse_dragged = false
		
		# Play sound for the selected fish
		if is_mouse_dragged:
			$AudioStreamPlayer2D_pick_up.play()

		vel_previous = (position - pos_previous)

	if event.is_action_released("left_click") and is_mouse_dragged:
		is_mouse_dragged = false
		
		# v = s/t (duh)
		vel = (position - pos_previous)/last_delta
		
		if vel.x < 0:
			is_facing_left = true
		elif vel.x > 0:
			is_facing_left = false
		
		# Make sure velocity does'nt get out of hand
		if vel.x > 1200:
			vel.x = 1200
		if vel.x < -1200:
			vel.x = -1200
		if vel.y > 1200:
			vel.y = 1200
		if vel.y < -1200:
			vel.y = -1200
	
	if event.is_action_pressed("right_click") and is_mouse_on_fish:
		death()


func _ready():
	""" Ready method """
	rng.randomize()
	update_size_and_animations()


func _process(delta):
	""" Process method """
	if is_menu_created:
		var cam = get_node("/root/Main/Camera2D")
		var menu = cam.get_children()[0].get_children()[0]
		position = menu.rect_global_position + Vector2(-200,200)*cam.zoom.x
	
	if not is_active:
		return
	
	turn_of_arrow_info_selected()

	""" ----- Set Behaviour ----- """
	FishBehaviourChoice.set_behaviour(self)
	
	""" -- Implement Behaviour -- """
	if behaviour == "do nothing": 
		do_nothing(delta)
	
	if behaviour == "random swim":
		idle_random_movement(delta)
	
	if behaviour == "shoal":
		move_as_shoal(delta)

	if behaviour == "acquire food":
		var food_list = acquire_food_list()
		attempt_to_eat_food(food_list)
	
	if behaviour == "acquire partner":
		if not chosen_partner:
			chosen_partner = search_for_partner()
		else:
			swim_to_kiss_partner(chosen_partner)

	if behaviour == "hunt":
		if not chosen_prey or not is_instance_valid(chosen_prey):
			chosen_prey = search_for_prey()
		else:
			swim_to_attack_prey(chosen_prey)
	
	if behaviour == "flee":
		attempt_to_flee()
	
	if behaviour == "defend":
		attempt_to_defend()

	if behaviour == "dead":
		death()

	if is_alive:
		manage_health_and_energy(delta)
		animate_right_left_switch()
		animate_fins()
	
	# Rotate back to zero
	if not is_looking_at_food:
		rotation = lerp_angle(rotation, 0, 0.05)
	
	""" ------ Movement ------ """
	# Fix world border
	if position.x <= 0:
		force_from_fins.x = fin_force
	elif position.x >= world_width:
		force_from_fins.x = - fin_force
	if position.y <= 0:
		force_from_fins.y = fin_force
	elif position.y >= abs_bottom_height:
		force_from_fins.y = - fin_force

	# Set force and add water resistance
	force.x = force_from_fins.x - water_resistance*vel.x*abs(vel.x)
	force.y = force_from_fins.y - water_resistance*vel.y*abs(vel.y)

	acc = force / mass_movement
	vel_previous = vel
	vel += acc

	if not is_mouse_dragged:
		var _collision = move_and_collide(vel * delta)			

	else:
		pos_previous = position
		position = get_global_mouse_position() - dragged_point
		last_delta = delta

	
func _on_behaviour_timer_timeout():
	reset_behaviour()

func reset_behaviour():
	FishBehaviourChoice.reset_behaviour(self)


func do_nothing(delta):
	""" Main method to control no movement """
	force_from_fins = Vector2.ZERO
	animate_idle(delta)


func idle_random_movement(delta):
	""" Main method to control idle movement force """
	if not is_idle_swimming:
		# Randomize the direction when idle swim is starting
		var angle = rng.randf_range(0, 2*PI)
		idle_force_direction = Vector2(cos(angle), sin(angle))
		is_idle_swimming = true

	force_from_fins = idle_force_direction*fin_force*rng.randf()
	animate_idle(delta)


func move_as_shoal(delta):
	""" Main method to control shoal movement force """
	var kin_list = acquire_detected_kin_list()
	
	if not kin_list.size() < 2:
		reset_behaviour()
		return
	
	# Get position in middle of two other kin fishes
	var target_position = (kin_list[0].position + kin_list[1].position) /2
	var target_direction = position.direction_to(target_position)

	force_from_fins = target_direction*fin_force
	animate_idle(delta)


func attempt_to_eat_food(food_list):
	""" Main method to control fish food acquirement """	
	# Se if there is any detected food items
	if not food_list:
		if is_eating:
			stop_looking_at_position()
		reset_behaviour()
		return
	
	var closest_food = locate_closest_food(food_list)

	move_to_closest_food(closest_food)

	var mouth_position = $Textures/mouth_top.global_position
	var distance_mouth_food = mouth_position.distance_to(closest_food.global_position)

	# Look at food if relatively close
	if distance_mouth_food <= (max(width,height)*2 + Global.fish_can_eat_distance):
		look_at_position(closest_food.global_position)
		is_looking_at_food = true
	
	# Eat food if even closer
	if distance_mouth_food <= (width/2 + Global.fish_can_eat_distance):
		eat_food(closest_food)
		is_eating = true
	
	if distance_mouth_food > (width/2 + Global.fish_can_eat_distance) and is_eating:
		is_eating = false
		is_looking_at_food = false
	
	if distance_mouth_food > (max(width,height)*2 + Global.fish_can_eat_distance) and is_looking_at_food:
		stop_looking_at_position()
		is_looking_at_food = false
	

func search_for_partner():
	var detected_kin = []
	for fish in detected_fishes:
		if FishSizeAttributesOutputs.is_fishes_kin(self, fish):
			detected_kin.append(fish)
	
	if detected_kin:
		var energy_factor = Global.fish_energy_kiss_factor
		var health_factor = Global.fish_health_kiss_factor
		for fish in detected_kin:
			if (gender == "female" and fish.gender == "male") or \
				(gender == "male" and fish.gender == "female"):
				if fish.energy >= fish.energy_max*energy_factor and\
					fish.health >= fish.health_max*health_factor and\
					position.distance_squared_to(fish.position) > pow(width, 0.25)*pow(height, 0.25)*5000 and\
					fish.is_alive and fish.is_active and not fish.is_pregnant and not fish.is_a_fish_child:
					# The fish cannot already have a partner
					if fish.chosen_partner == null:
						fish.chosen_partner = self
						fish.behaviour = "acquire partner"
						var partner = fish
						return partner # Break loop if a partner is found
		return null # Reset if no partner is chosen

	else:
		return null	# Reset if no kin is detected

	
func swim_to_kiss_partner(partner):

	if not is_instance_valid(partner):
		reset_behaviour()
		return

	if not is_alive or not partner.is_alive:
		reset_behaviour()
		return
	 
	var direction_to_partner = position.direction_to(partner.position)
	var distance_to_partner = position.distance_to(partner.position)

	force_from_fins = direction_to_partner*fin_force
	
	if distance_to_partner <= width*8:
		force_from_fins = direction_to_partner*fin_force*0.4
		look_at_position(partner.get_node("Textures/mouth_inside").global_position)
	if distance_to_partner <= width*4:
		force_from_fins = direction_to_partner*fin_force*0.05
	if distance_to_partner <= width*2:
		kiss()
		partner.kiss()


func kiss():
	if is_kissing:
		return
	# Stop and kiss
	is_kissing = true
	vel = Vector2.ZERO
	$anim_mouth.play("kiss")
	$AudioStreamPlayer2D_kiss.play()
	emit_particles_kiss()
	
func _on_anim_mouth_animation_finished(anim_name):
	if anim_name == "kiss":
		is_kissing = false
		if rng.randf() < 0.8 and gender == "female":
			get_pregnant()
			father_of_child = chosen_partner

		chosen_partner = null
		energy = energy*0.8
		reset_behaviour()
		

func get_pregnant():
	is_pregnant = true
	var pregnant_timer = Timer.new()
	pregnant_timer.connect("timeout",self,"_on_pregnant_timer_timeout") 
	pregnant_timer.one_shot = true
	add_child(pregnant_timer)
	pregnant_timer.start(Global.fish_pregnant_time)

func _on_pregnant_timer_timeout():
	var fish_scene = load("res://fish/Fish.tscn")
	var fish_instance = fish_scene.instance()

	if is_facing_left:
		fish_instance.position = Vector2(position.x+width*2, position.y+height*0.5)
	else:
		fish_instance.position = Vector2(position.x-width*2, position.y+height*0.5)
	fish_instance.set_attributes_from_parents(self, father_of_child)
	
	get_node("/root/Main/Fishes").add_child(fish_instance)
	fish_instance.has_just_been_born()
	$AudioStreamPlayer2D_birth.play()


func has_just_been_born():
	scale = Vector2(0.5,0.5)
	is_a_fish_child = true
	health = 0.8*health_max
	energy = 0.5*energy_max
	var growth_timer = Timer.new()
	growth_timer.connect("timeout",self,"_on_growth_timer_timeout") 
	growth_timer.one_shot = true
	add_child(growth_timer)
	growth_timer.start(Global.fish_child_growth_time)

func _on_growth_timer_timeout():
	if not is_alive:
		return
	
	is_a_fish_child = false
	scale = Vector2(1,1)


func search_for_prey():
	if not detected_fishes:
		reset_behaviour()
		return null
	
	var closest_fish = null
	for fish in detected_fishes:
		if fish.is_active and fish.is_alive and not fish.is_menu_created and\
			not FishSizeAttributesOutputs.is_fishes_kin(self, fish):
			if closest_fish:
				if global_position.distance_to(fish.position) < global_position.distance_to(closest_fish.position):
					closest_fish = fish
			else:
				closest_fish = fish
	
	reset_behaviour()
	return closest_fish


func swim_to_attack_prey(prey):
	is_hunting = true
	if not prey:
		reset_behaviour()
		return
	
	if not prey.is_alive:
		prey = null
		reset_behaviour()
		return
	 
	var direction_to_prey = position.direction_to(prey.position)
	var distance_to_prey = position.distance_to(prey.position)

	force_from_fins = direction_to_prey*fin_force
	look_at_position(prey.position)
	
	if distance_to_prey <= prey.eye_size*Global.fish_detect_attacker_multiplyer and not prey.is_attacked:
		# The prey gets a chance to notice the attacking fish and pick a new behaviour
		prey.attacking_fish = self
		prey.reset_behaviour()
		prey.is_attacked = true

	if distance_to_prey <= width*2:
		if not $anim_mouth.is_playing():
			$anim_mouth.play("mouth_attack")
			emit_particles_attack()
			$AudioStreamPlayer2D_attack.play()
			
			prey.health -= teeth_size*Global.fish_attack_multiplyer
			energy -= teeth_size*Global.fish_attack_energy_drain_multiplyer
			# Each attack allows the prey to pick a new strategy
			prey.reset_behaviour()
			prey.is_attacked = true


func attempt_to_flee():
	if not attacking_fish or not is_instance_valid(attacking_fish):
		attacking_fish = null
		reset_behaviour()
		return
	
	if not attacking_fish.is_alive:
		attacking_fish = null
		reset_behaviour()
		return
	
	var direction_to_attacker = position.direction_to(attacking_fish.position)
	var opposite_direction = Vector2(-direction_to_attacker.x, -direction_to_attacker.y)
	
	# A bit to elegant solution to vector math problem
	look_at_position(position + opposite_direction)
	force_from_fins = opposite_direction*fin_force


func attempt_to_defend():
	if not attacking_fish or not is_instance_valid(attacking_fish):
		attacking_fish = null
		reset_behaviour()
		return
	
	if not attacking_fish.is_alive:
		attacking_fish = null
		reset_behaviour()
		return
	
	var distance_to_attacker = position.distance_to(attacking_fish.position)
	force_from_fins = Vector2.ZERO
	
	look_at_position(attacking_fish.position)
	if distance_to_attacker <= width*2:
		if not $anim_mouth.is_playing():
			$anim_mouth.play("mouth_attack")
			emit_particles_attack()
			
			attacking_fish.health -= teeth_size*Global.fish_attack_multiplyer
			energy -= teeth_size*Global.fish_attack_energy_drain_multiplyer


func death():
	""" Main method for death """
	if is_alive:
		is_alive = false
		$anim_death.play("death_changes")
		$Sliders.visible = false
		$AudioStreamPlayer2D_death.play()
	force_from_fins = Vector2(0, -mass*0.01)

	
func acquire_food_list():
	var food_list = []
	if "berries" in food_capability:
		food_list += detected_berries
	if "carrots" in food_capability:
		food_list += detected_carrots
	if "fishes" in food_capability:
		var detected_dead_fishes = []
		for fish in detected_fishes:
			if fish: # Avoid null instace bug
				if not fish.is_alive:
					detected_dead_fishes.append(fish)
		food_list += detected_dead_fishes
	
	return food_list


func acquire_detected_kin_list():
	var kin_list = []
	for fish in detected_fishes:
		if FishSizeAttributesOutputs.is_fishes_kin(self, fish):
			kin_list.append(fish)
	return kin_list


func locate_closest_food(food_list):
	var closest_food = food_list[0]

	# only run loop if there is multiple food items
	if food_list.size() > 1:
		var distance_to_closest_food = position.distance_to(closest_food.global_position)
		for food in food_list:
			if position.distance_to(food.global_position) <= distance_to_closest_food:
				closest_food = food
				distance_to_closest_food = position.distance_to(closest_food.global_position)
	return closest_food

	
func move_to_closest_food(closest_food):
	var direction_to_food = position.direction_to(closest_food.global_position)

	# Fished dont push as much while eating
	var eat_force = fin_force
	if is_eating:
		eat_force = fin_force*0.1

	force_from_fins = direction_to_food*eat_force*rng.randf()
	

func look_at_position(other_pos):
	if not other_pos:
		rotation = 0
		return

	var direction_to_object = position.direction_to(other_pos)
	var angle_to_object = direction_to_object.angle()

	
	# Smooth rotation to value and scale to get right side up
	if direction_to_object.x > 0:
		rotation = lerp_angle(rotation, angle_to_object, 0.05)
		$Textures.scale.x = -0.08
		$Textures.scale.y = 0.08
		is_facing_left = false
	else:
		rotation = lerp_angle(rotation, angle_to_object+PI, 0.05)
		$Textures.scale.x = 0.08
		$Textures.scale.y = 0.08
		is_facing_left = true

func stop_looking_at_position():
	$Textures.scale.y = 0.08

	if vel.x <= 0:
		$Textures.scale.x = 0.08
		is_facing_left = true
	else:
		$Textures.scale.x = -0.08
		is_facing_left = false

	
func eat_food(food):
	if not $anim_mouth.is_playing():
		$anim_mouth.play("eat_food")
		var eat_animation_time = Global.fish_eat_animation_time

		food.gets_eaten(mouth_size*eat_animation_time)
		if energy < energy_max:
			energy += mouth_size*Global.fish_eat_energy_factor*energy_max*eat_animation_time
		elif energy >= energy_max:
			energy = energy_max

	
func manage_health_and_energy(delta):
	""" Main method to control fish helth and energy """
	
	# Heal if energy is +50%
	if health < health_max and energy > energy_max*0.5:
		health += heal_per_second_percent*0.01*health_max *delta
		energy -= heal_per_second_percent*0.01*health_max *delta
	
	if energy > energy_max*Global.fish_low_energy_level:
		# Lower energy drain if not moving
		if force_from_fins == Vector2.ZERO:
			energy -= energy_drain*energy_drain_multiplyer*delta*0.75
		else:
			energy -= energy_drain*energy_drain_multiplyer*delta
	
	if energy <= energy_max*Global.fish_low_energy_level:
		health -= Global.fish_low_energy_damage*delta
	
	# Update health and energy bars only here
	health_bar.value = health
	energy_bar.value = energy


func update_size_and_animations():
	# Set outputs based on attrubutes
	set_outputs()
	# Set size based on attributes
	update_size()
	# Fill animationplayers with animations
	CreateFishAnimations.create_all_animations(self)

	set_body_fins_color()
	set_detection_area_size()
	
	if not is_menu_created:
		health_bar.max_value = health_max
		health_bar.value = health
		
		energy_bar.max_value = energy_max
		energy_bar.value = energy


func set_outputs():
	FishSizeAttributesOutputs.set_outputs(self)


func update_size():
	FishSizeAttributesOutputs.update_size(self)


func get_attributes_from_create_menu():
	CreateFishFromMenu.set_attributes(self)


func randomize_fish_start_attributes():
	FishSizeAttributesOutputs.randomize_start_attributes(self)


func set_attributes_from_parents(mother, father):
	FishSizeAttributesOutputs.set_attributes_from_parents(self, mother, father)


func set_detection_area_size():
	$detection_area.scale = Vector2(1,1)*eye_size*Global.fish_detection_multiplyer


func animate_idle(delta):
	if not $anim_idle.is_playing():
		if rng.randf() < 0.08*delta:
			$anim_idle.play("rotate_eyes")
		if rng.randf() < 0.1*delta:
			$anim_idle.play("rotate_mouth")


func animate_fins():
	if abs(force.x) > 0 or abs(force.y) > 0 and not $anim_fins.is_playing():
		$anim_fins.set_speed_scale(0.6 + abs(acc.x))
		$anim_fins.play("swim_normal")

func animate_right_left_switch():
	if force_from_fins.x > 0 and vel.x > 0 and is_facing_left and not is_looking_at_food:
		is_facing_left = false
		$anim_left_right.play("left_right_switch")
	elif force_from_fins.x < 0 and vel.x < 0 and not is_facing_left and not is_looking_at_food:
		is_facing_left = true
		$anim_left_right.play_backwards("left_right_switch")

	if is_mouse_dragged and not is_looking_at_food:
		if (position.x-pos_previous.x) > 0 and $Textures.scale.x > 0:
			$anim_left_right.play("left_right_switch")
		elif (position.x-pos_previous.x) < 0 and $Textures.scale.x < 0:
			$anim_left_right.play_backwards("left_right_switch")


func emit_particles_kiss():
	$Textures/mouth_inside/particles_kiss.emitting = true


func emit_particles_attack():
	$Textures/mouth_inside/particles_attack.emitting = true


func toggel_health_energy_bars():
	if is_alive:
		$Sliders.visible = !$Sliders.visible


func gets_eaten(bite_size):
	food_energy_value -= bite_size

	if food_energy_value <= 0:
		queue_free()
	elif food_energy_value <= food_energy_value_max*0.25:
		$Textures/body.set_frame(3)
	elif food_energy_value <= food_energy_value_max*0.5:
		$Textures/body.set_frame(2)
	elif food_energy_value <= food_energy_value_max*0.75:
		$Textures/body.set_frame(1)


func turn_of_arrow_info_selected():
	# Set info selected ring around the info selected fish
	if not is_info_selected:
		info_selected_arrow.visible = false


func turn_on_arrow_info_selected():
	# Set info selected ring around the info selected fish
	if is_info_selected:
		info_selected_arrow.visible = true


func set_body_fins_color():
	FishSizeAttributesOutputs.set_body_fins_color(self)


func not_colliding_timer_sec(time):
	$CollisionShape2D.disabled = true
	var not_colliding_timer = Timer.new()

	not_colliding_timer.connect("timeout",self,"_on_not_colliding_timer_timeout") 
	not_colliding_timer.set_wait_time(time)
	not_colliding_timer.one_shot = true
	
	add_child(not_colliding_timer)
	not_colliding_timer.start()

func _on_not_colliding_timer_timeout():
	$CollisionShape2D.disabled = false


func _on_detection_area_body_entered(entering_body):
	if entering_body.is_in_group("fishes"):
		if entering_body.get_index() != get_index():
			detected_fishes.append(entering_body)
	if entering_body.is_in_group("carrots"):
		detected_carrots.append(entering_body)
	if entering_body.is_in_group("berries"):
		detected_berries.append(entering_body)

func _on_detection_area_body_exited(entering_body):
	if entering_body.is_in_group("fishes"):
		detected_fishes.erase(entering_body)
	if entering_body.is_in_group("carrots"):
		detected_carrots.erase(entering_body)
	if entering_body.is_in_group("berries"):
		detected_berries.erase(entering_body)


func _on_Fish_mouse_entered():
	is_mouse_on_fish = true
func _on_Fish_mouse_exited():
	is_mouse_on_fish = false
