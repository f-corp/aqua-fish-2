extends KinematicBody2D

var fish_name = ""

# Movement
var force_from_fins = Vector2(0,0)

var force = Vector2(0,0)
var acc = Vector2(0,0)
var vel = Vector2(0,0)

var vel_previous = Vector2(0,0)
var pos_previous = Vector2(0,0)
var idle_force_direction = Vector2(0,0)

var is_alive = true
var is_eating = false
var is_looking_at_food = false
var is_facing_left = true
var is_idle_swimming = false
var is_pregnant = false
var is_a_fish_child = false
var is_hunting = false
var is_attacked = true
var is_kissing = false

var is_active = true
var is_menu_created = false
var is_info_selected = false
var is_mouse_dragged = false
var is_mouse_on_fish = false

var chosen_partner = null
var chosen_prey = null
var attacking_fish = null
var father_of_child = null

# Fish Attributes
var height = 10
var width = 10
var mass = 100
var mass_movement = 36.0
var eye_size = 10
var fin_size = 10
var mouth_size = 10
var teeth_size = 10
var gender: String = "female"
var aggressive = 10
var nervous = 10
var hunger = 10
var passion = 10
var selfish = 10
var conforming = 10
var mutable = 10

# Fish Outputs
var fin_force
var water_resistance
var health
var health_max
var energy
var energy_max
var energy_drain
var eat_speed
var food_energy_value
var food_energy_value_max
var food_capability = []
var awareness
var attack
var defence

# Fish inner workings
var heal_per_second_percent = Global.fish_heal_per_second_percent
var energy_drain_multiplyer = Global.fish_energy_drain_multiplyer

# Misc
var rng = RandomNumberGenerator.new()
var last_delta = 0
var dragged_point = Vector2.ZERO
var is_in_main_menu = false

var world_width = OS.get_screen_size().x
var world_height = OS.get_screen_size().y

onready var health_bar = $Sliders/HealthBar
onready var energy_bar = $Sliders/EnergyBar


func _input(event):
	""" All inputs """
	if event.is_action_pressed("left_click") and fish_name == "jens1":
		is_mouse_dragged = true
		dragged_point = get_global_mouse_position() - global_position

		if dragged_point.length() > 50:
			is_mouse_dragged = false

		vel_previous = (position - pos_previous)

	if event.is_action_released("left_click") and is_mouse_dragged:
		is_mouse_dragged = false
		
		# v = s/t (duh)
		vel = (position - pos_previous)/last_delta
		
		if vel.x < 0:
			is_facing_left = true
		elif vel.x > 0:
			is_facing_left = false
		
		# Make sure velocity does'nt get out of hand
		if vel.x > 1200:
			vel.x = 1200
		if vel.x < -1200:
			vel.x = -1200
		if vel.y > 1200:
			vel.y = 1200
		if vel.y < -1200:
			vel.y = -1200


func _ready():
	""" Ready method """
	rng.randomize()
	update_size_and_animations()


func _process(delta):
	""" Process method """
	if not is_active:
		return
		
	# Rotate back to zero
	if not is_looking_at_food:
		rotation = lerp_angle(rotation, 0, 0.05)
	
	""" ------ Movement ------ """
	# Fix world border
	if position.x <= 0:
		force_from_fins.x = fin_force
	elif position.x >= world_width:
		force_from_fins.x = - fin_force
	if position.y <= 0:
		force_from_fins.y = fin_force
	elif position.y >= world_height:
		force_from_fins.y = - fin_force
	
	# make dragged fish go back
	if fish_name == "jens1":
		if position.distance_to(Vector2(world_width*0.1, world_height*0.5)) > 50:
			if position.x < world_width*0.1:
				force_from_fins.x = fin_force
			elif position.x > world_width*0.1:
				force_from_fins.x = - fin_force
			if position.y < world_height*0.5:
				force_from_fins.y = fin_force
			elif position.y > world_height*0.5:
				force_from_fins.y = - fin_force
		else:
			force_from_fins = Vector2.ZERO

	# Set force and add water resistance
	force.x = force_from_fins.x - water_resistance*vel.x*abs(vel.x)
	force.y = force_from_fins.y - water_resistance*vel.y*abs(vel.y)

	acc = force / mass_movement
	vel_previous = vel
	vel += acc

	animate_right_left_switch()
	animate_idle(delta)
	animate_fins()

	if not is_mouse_dragged:
		var _collision = move_and_collide(vel * delta)			

	else:
		pos_previous = position
		position = get_global_mouse_position() - dragged_point
		last_delta = delta		


func do_nothing(delta):
	""" Main method to control no movement """
	force_from_fins = Vector2.ZERO
	animate_idle(delta)


func idle_random_movement(delta):
	""" Main method to control idle movement force """
	if not is_idle_swimming:
		# Randomize the direction when idle swim is starting
		var angle = rng.randf_range(0, 2*PI)
		idle_force_direction = Vector2(cos(angle), sin(angle))
		is_idle_swimming = true

	force_from_fins = idle_force_direction*fin_force*rng.randf()
	animate_idle(delta)


func has_just_been_born():
	scale = Vector2(0.5,0.5)
	is_a_fish_child = true
	health = 0.8*health_max
	energy = 0.5*energy_max
	var growth_timer = Timer.new()
	growth_timer.connect("timeout",self,"_on_growth_timer_timeout") 
	growth_timer.one_shot = true
	add_child(growth_timer)
	growth_timer.start(Global.fish_child_growth_time)

func _on_growth_timer_timeout():
	if not is_alive:
		return
	
	is_a_fish_child = false
	scale = Vector2(1,1)



func death():
	""" Main method for death """
	if is_alive:
		is_alive = false
		$anim_death.play("death_changes")
		$Sliders.visible = false
		$AudioStreamPlayer2D_death.play()
	force_from_fins = Vector2(0, -mass*0.01)


func look_at_position(other_pos):
	if not other_pos:
		rotation = 0
		return

	var direction_to_object = position.direction_to(other_pos)
	var angle_to_object = direction_to_object.angle()

	
	# Smooth rotation to value and scale to get right side up
	if direction_to_object.x > 0:
		rotation = lerp_angle(rotation, angle_to_object, 0.05)
		$Textures.scale.x = -0.08
		$Textures.scale.y = 0.08
		is_facing_left = false
	else:
		rotation = lerp_angle(rotation, angle_to_object+PI, 0.05)
		$Textures.scale.x = 0.08
		$Textures.scale.y = 0.08
		is_facing_left = true

func stop_looking_at_position():
	$Textures.scale.y = 0.08

	if vel.x <= 0:
		$Textures.scale.x = 0.08
		is_facing_left = true
	else:
		$Textures.scale.x = -0.08
		is_facing_left = false

	
func eat_food(food):
	if not $anim_mouth.is_playing():
		$anim_mouth.play("eat_food")
		var eat_animation_time = Global.fish_eat_animation_time

		food.gets_eaten(mouth_size*eat_animation_time)
		if energy < energy_max:
			energy += mouth_size*Global.fish_eat_energy_factor*energy_max*eat_animation_time
		elif energy >= energy_max:
			energy = energy_max

	
func manage_health_and_energy(delta):
	""" Main method to control fish helth and energy """
	
	# Heal if energy is +50%
	if health < health_max and energy > energy_max*0.5:
		health += heal_per_second_percent*0.01*health_max *delta
		energy -= heal_per_second_percent*0.01*health_max *delta
	
	if energy > energy_max*Global.fish_low_energy_level:
		# Lower energy drain if not moving
		if force_from_fins == Vector2.ZERO:
			energy -= energy_drain*energy_drain_multiplyer*delta*0.75
		else:
			energy -= energy_drain*energy_drain_multiplyer*delta
	
	if energy <= energy_max*Global.fish_low_energy_level:
		health -= Global.fish_low_energy_damage*delta
	
	# Update health and energy bars only here
	health_bar.value = health
	energy_bar.value = energy


func update_size_and_animations():
	# Set outputs based on attrubutes
	set_outputs()
	# Set size based on attributes
	update_size()
	# Fill animationplayers with animations
	CreateFishAnimations.create_all_animations(self)

	set_body_fins_color()
	
	if not is_menu_created:
		health_bar.max_value = health_max
		health_bar.value = health
		
		energy_bar.max_value = energy_max
		energy_bar.value = energy


func set_outputs():
	FishSizeAttributesOutputs.set_outputs(self)


func update_size():
	FishSizeAttributesOutputs.update_size(self)


func animate_idle(delta):
	if not $anim_idle.is_playing():
		if rng.randf() < 0.08*delta:
			$anim_idle.play("rotate_eyes")
		if rng.randf() < 0.1*delta:
			$anim_idle.play("rotate_mouth")


func animate_fins():
	if abs(force.x) > 0 or abs(force.y) > 0 and not $anim_fins.is_playing():
		$anim_fins.set_speed_scale(0.6 + abs(acc.x))
		$anim_fins.play("swim_normal")


func animate_right_left_switch():
	if not is_mouse_dragged:
		if force_from_fins.x > 0 and vel.x > 0 and is_facing_left:
			is_facing_left = false
			$anim_left_right.play("left_right_switch")
		elif force_from_fins.x < 0 and vel.x < 0 and not is_facing_left:
			is_facing_left = true
			$anim_left_right.play_backwards("left_right_switch")

	else:
		if (position.x-pos_previous.x) > 0 and is_facing_left:
			is_facing_left = false
			$anim_left_right.play("left_right_switch")
		elif (position.x-pos_previous.x) < 0 and not is_facing_left:
			is_facing_left = true
			$anim_left_right.play_backwards("left_right_switch")


func emit_particles_kiss():
	$Textures/mouth_inside/particles_kiss.emitting = true


func emit_particles_attack():
	$Textures/mouth_inside/particles_attack.emitting = true


func toggel_health_energy_bars():
	if is_alive:
		$Sliders.visible = !$Sliders.visible

	
func set_body_fins_color():
	FishSizeAttributesOutputs.set_body_fins_color(self)


func _on_Fish_mouse_entered():
	is_mouse_on_fish = true
func _on_Fish_mouse_exited():
	is_mouse_on_fish = false
