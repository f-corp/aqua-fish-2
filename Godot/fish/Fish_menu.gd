extends KinematicBody2D


# Movement
var force_from_fins = Vector2(0,0)

var force = Vector2(0,0)
var acc = Vector2(0,0)
var vel = Vector2(0,0)

var vel_previous = Vector2(0,0)
var pos_previous = Vector2(0,0)
var idle_force_direction = Vector2(0,0)

var is_facing_left = true
var movement_angle = 0

# Fish Attributes
var height = 10
var width = 10
var mass = 100
var mass_movement = 36
var eye_size = 10
var fin_size = 10
var mouth_size = 10
var teeth_size = 10
var gender = "female"
var aggressive = 10
var nervous = 10
var hunger = 10
var passion = 10
var selfish = 10
var conforming = 10
var mutable = 10

# Fish Outputs
var fin_force
var water_resistance
var health
var health_max
var energy
var energy_max
var energy_drain
var eat_speed
var food_energy_value
var food_energy_value_max
var food_capability = []
var awareness
var attack
var defence


# Misc
var is_in_main_menu = true
var rng = RandomNumberGenerator.new()

var world_width = OS.get_screen_size().x
var world_height = OS.get_screen_size().y


func _ready():
	""" Ready method """
	rng.randomize()
	randomize_fish_start_attributes()
	update_size_and_animations()
	randomize_movement_direction()


func _process(delta):
	""" Process method """
	
	idle_random_movement(delta)
	
	animate_right_left_switch()
	animate_fins()
	
	""" ------ Movement ------ """
	# Fix world border
	if position.x <= 0:
		force_from_fins.x = fin_force
	elif position.x >= world_width:
		force_from_fins.x = - fin_force
	if position.y <= 0:
		force_from_fins.y = fin_force
	elif position.y >= world_height:
		force_from_fins.y = - fin_force

	# Set force and add water resistance
	force.x = force_from_fins.x - water_resistance*vel.x*abs(vel.x)
	force.y = force_from_fins.y - water_resistance*vel.y*abs(vel.y)

	acc = force / mass_movement
	vel_previous = vel
	vel += acc

	var _collision = move_and_collide(vel * delta)			


func randomize_movement_direction():
	movement_angle = rng.randf_range(0, 2*PI)


func idle_random_movement(delta):
	""" Main method to control idle movement force """
	idle_force_direction = Vector2(cos(movement_angle), sin(movement_angle))

	force_from_fins = idle_force_direction*fin_force*rng.randf()
	animate_idle(delta)


func update_size_and_animations():
	# Set outputs based on attrubutes
	set_outputs()
	# Set size based on attributes
	update_size()
	# Fill animationplayers with animations
	CreateFishAnimations.create_all_animations(self)


func set_outputs():
	FishSizeAttributesOutputs.set_outputs(self)


func update_size():
	FishSizeAttributesOutputs.update_size(self)


func get_attributes_from_create_menu():
	CreateFishFromMenu.set_attributes(self)


func randomize_fish_start_attributes():
	FishSizeAttributesOutputs.randomize_start_attributes(self)


func animate_idle(delta):
	if not $anim_idle.is_playing():
		if rng.randf() < 0.08*delta:
			$anim_idle.play("rotate_eyes")
		if rng.randf() < 0.1*delta:
			$anim_idle.play("rotate_mouth")


func animate_fins():
	if abs(force.x) > 0 or abs(force.y) > 0 and not $anim_fins.is_playing():
		$anim_fins.set_speed_scale(0.6 + abs(acc.x))
		$anim_fins.play("swim_normal")

func animate_right_left_switch():
	if force_from_fins.x > 0 and vel.x > 0 and is_facing_left:
		is_facing_left = false
		$anim_left_right.play("left_right_switch")
	elif force_from_fins.x < 0 and vel.x < 0 and not is_facing_left:
		is_facing_left = true
		$anim_left_right.play_backwards("left_right_switch")


func _on_behaviour_timer_timeout():
	randomize_movement_direction()