extends Node2D

onready var blur = $Background/Blur
var blur_amount = 0

# system variables
var screen_size = OS.get_screen_size()
var rng = RandomNumberGenerator.new()
var is_press_space_text_visible = false

var world_size_mult = Global.world_size_screen_multiple
var world_width = screen_size.x*world_size_mult
var world_height = screen_size.y*world_size_mult

# world variables
var bottom_height = world_height*(1-Global.world_floor_multiple)
var abs_bottom_height = world_height*(1-Global.world_floor_absolute_bottom)
var stone_amount = Global.world_stone_amount
var stone_back_proportion = Global.world_stone_back_proportion

# Carrot variables
var carrot_timer_min = Global.carrot_drop_timer_min
var carrot_timer_max = Global.carrot_drop_timer_max


func _input(event):

	CameraMovement.check_move_and_zoom_controls($Camera2D, event)

	if event.is_action_released("add_carrot"):
		add_carrot()
	
	if event.is_action_pressed('toggle_fullscreen') and not OS.has_feature("HTML5"):
		OS.window_fullscreen = !OS.window_fullscreen

	if event is InputEventMouseMotion:
		# Start delay timer for mouse fade out
		$PressSpaceFadeOutTimer.start()
		if not is_press_space_text_visible:
			$AnimationPlayerPressSpace.play("TextIn") # Fade in if it's not allready

	if event.is_action_released("open_menu"):
		if not Global.menu_visable:
			Global.menu_visable = true
			var r_click_menu_scene = load("res://UI/Menu_base.tscn")
			var r_click_menu_instance = r_click_menu_scene.instance()
			$Camera2D/Menus.add_child(r_click_menu_instance)
		elif Global.menu_visable:
			Global.menu_visable = false
			Global.menu_fish_info_visable = false
			Global.menu_is_mouse_inside_menues = false
			# Remove menus
			for child in $Camera2D/Menus.get_children():
				child.queue_free()
			# Remove any created fish
			for fish in $Fishes.get_children():
				fish.is_info_selected = false
				if fish.is_menu_created:
					fish.queue_free()


func _ready():
	# Initialize all options changes
	Engine.time_scale = Global.simulation_time_scale
	CameraMovement.reset_global_values()
	Plants.reset_global_values()
	Stones.reset_global_values()
	Statistics.reset_global_values()

	# Set window size to screen size
	OS.window_fullscreen = true
	OS.set_window_size(Vector2(world_width, world_height))
	$Background.position = Vector2(world_width/2,world_height/2)

	# Make sure backgound looks good on all monitors
	var background_scale_x = world_size_mult * screen_size.x/1920
	var background_scale_y = world_size_mult * screen_size.y/1080
	$Background.scale = Vector2(background_scale_x, background_scale_y)

	# Set up camera
	$Camera2D.position = Vector2(world_width, world_height)/2
	var start_zoom_value = world_size_mult*0.8
	$Camera2D.zoom = Vector2(start_zoom_value, start_zoom_value)
	CameraMovement.scale_volume(start_zoom_value)

	set_up_press_space_label()
	
	# New randomize
	rng.randomize()
	$Statistics_save_timer.wait_time = Global.statistics_time_intervals
	
	# Create fishes
	CreateFishesStart.create_fishes(self)

	# Place shipwreck
	var ship_x_pos = world_width*(0.75 + rng.randf_range(0,0.15))
	var ship_y_pos = world_height*0.77
	$Shipwreck.position = Vector2(ship_x_pos, ship_y_pos)

	# Place stone pile
	var stone_pile_x_pos = world_width*(0.1 + rng.randf_range(0, 0.15))
	var stone_pile_y_pos = world_height*0.78
	$Stones/Stone_pile.position = Vector2(stone_pile_x_pos, stone_pile_y_pos)

	# Place stones
	var stones_back = Stones.add_stones_back()
	for stone in stones_back:
		$Stones/Back.add_child(stone)
	
	var stones_front = Stones.add_stones_front()
	for stone in stones_front:
		$Stones/Front.add_child(stone)
	
	# Place berry plants
	var berry_plants = Plants.add_berry_plants()
	for plant in berry_plants:
		$Berry_plants.add_child(plant)
	
	# Place estetic plants
	var front_plants = Plants.add_estetic_front_plants()
	for plant in front_plants:
		$Front_plants.add_child(plant)
	
	var back_plants = Plants.add_estetic_back_plants()
	for plant in back_plants:
		$Back_plants.add_child(plant)

	# Initialize carrot drops
	carrot_timer_start()

	# Intitial WhiteBLur
	$Background/White.visible = true
	$AnimationPlayerStart.play("WhiteBlur")

	play_and_set_ambient_bubble()


func _process(_delta):
	CameraMovement.move_camera_if_mouse_at_border($Camera2D, get_local_mouse_position())
	
	# Set camera blur when zoomed close in
	if $Camera2D.zoom.x <= 2.0:
		blur_amount = 5 - $Camera2D.zoom.x*2.5
		blur.material.set_shader_param("blur_amount", blur_amount)
	elif $Camera2D.zoom.x > 2.0 and blur_amount != 0:
		blur_amount = 0
		blur.material.set_shader_param("blur_amount", blur_amount)
	
	if OS.has_feature("HTML5"):
		OS.window_fullscreen = true


func carrot_timer_start():
	# Set timer based on global vals
	var new_carrot_time = rng.randf_range(carrot_timer_min, carrot_timer_max)
	var new_carrot_timer = Timer.new()

	new_carrot_timer.connect("timeout",self,"_on_carrot_timer_timeout") 
	new_carrot_timer.set_wait_time(new_carrot_time)
	new_carrot_timer.one_shot = true
	
	add_child(new_carrot_timer)
	new_carrot_timer.start()
	
func _on_carrot_timer_timeout():
	# Drop carrot and reset timer at timeut
	add_carrot()
	carrot_timer_start()

func add_carrot():
	# Instance carrots at surface
	var carrot_scene = load("res://world/food/Carrot.tscn")
	var carrot_instance = carrot_scene.instance()

	$Carrots.add_child(carrot_instance)


func set_up_press_space_label():
	$Camera2D/Text/PressSpace.rect_position.y = screen_size.y*0.92*0.5
	$Camera2D/Text/PressSpace.rect_position.x = -100
	# Invisible from start
	$Camera2D/Text/PressSpace.modulate = Color(0,0,0,0)


func _on_Statistics_save_timer_timeout():
	Statistics.save_mean_values_to_list($Fishes)


func play_and_set_ambient_bubble():
	$Timer_ambient_bubble.wait_time = rng.randf_range(20,30)
	$Timer_ambient_bubble.start()
	
	
func _on_Timer_ambient_bubble_timeout():
	$AudioStreamPlayer_ambient_bubble.play()
	play_and_set_ambient_bubble()


func _on_PressSpaceFadeOutTimer_timeout():
	$AnimationPlayerPressSpace.play("TextOut")
	is_press_space_text_visible = false

func _on_AnimationPlayerPressSpace_animation_finished(anim):
	if anim == "TextIn":
		is_press_space_text_visible = true
