extends Node

# system variables
var screen_size = OS.get_screen_size()
var rng = RandomNumberGenerator.new()

var world_size_mult = Global.world_size_screen_multiple
var world_width = screen_size.x*world_size_mult
var world_height = screen_size.y*world_size_mult


# world variables
var bottom_height = world_height*(1-Global.world_floor_multiple)
var abs_bottom_height = world_height*(1-Global.world_floor_absolute_bottom)


# ---------- Place berry plants ----------
func add_berry_plants():

	var berry_plant_scene1 = load("res://world/plants/Berry_plant1.tscn")
	var berry_plant_scene2 = load("res://world/plants/Berry_plant2.tscn")
	var berry_plant_scene3 = load("res://world/plants/Berry_plant3.tscn")

	var berry_plant_amount = Global.berry_plant_amount
	var berry_plant_output = []
	var plants_x_pos = []
	var plants_to_close = false

	for _i in range(berry_plant_amount):
		# Randomize which plant model is choosen
		rng.randomize()
		var rand = rng.randf()

		var scene = berry_plant_scene1
		if rand < 0.3:
			scene = berry_plant_scene2
		elif rand < 0.6:
			scene = berry_plant_scene3
		
		# Instance and set up plant
		var plant_instance = scene.instance()
		var x_pos = rng.randi_range(20,world_width-20)
		var y_pos = rng.randi_range(bottom_height,int(bottom_height*1.05))
		if plants_x_pos:
			plants_to_close = true
		while plants_to_close:
			plants_to_close = false
			for other_plant_x in plants_x_pos:
				if abs(other_plant_x - x_pos) < 170:
					plants_to_close = true
					x_pos = rng.randi_range(20, (world_width-20))
					continue

		plant_instance.position = Vector2(x_pos, y_pos)
		berry_plant_output.append(plant_instance)
	
	# Return a list of plants
	return berry_plant_output


# ---------- Place estetic front plants ----------
func add_estetic_front_plants():

	var front_plant_scene1 = load("res://world/plants/Plant_estetic1.tscn")
	var front_plant_scene2 = load("res://world/plants/Plant_estetic2.tscn")
	var front_plant_scene3 = load("res://world/plants/Plant_estetic3.tscn")

	var front_plant_amount = Global.estetic_front_plant_amount
	var front_plant_output = []
	var plants_x_pos = []
	var plants_to_close = false

	for _i in range(front_plant_amount):
		# Randomize which plant model is choosen
		rng.randomize()
		var rand = rng.randf()

		var scene = front_plant_scene1
		if rand < 0.3:
			scene = front_plant_scene2
		elif rand < 0.6:
			scene = front_plant_scene3
		
		# Instance and set up plant
		var plant_instance = scene.instance()
		var x_pos = rng.randi_range(20,world_width-20)
		var y_pos = rng.randi_range(abs_bottom_height, int(abs_bottom_height*1.05))
		if plants_x_pos:
			plants_to_close = true
		while plants_to_close:
			plants_to_close = false
			for other_plant_x in plants_x_pos:
				if abs(other_plant_x - x_pos) < 170:
					plants_to_close = true
					x_pos = rng.randi_range(20, (world_width-20))
					continue

		plant_instance.position = Vector2(x_pos, y_pos)
		front_plant_output.append(plant_instance)
	
	# Return a list of plants
	return front_plant_output

# ---------- Place estetic front plants ----------
func add_estetic_back_plants():

	var back_plant_scene1 = load("res://world/plants/Plant_estetic1.tscn")
	var back_plant_scene2 = load("res://world/plants/Plant_estetic2.tscn")
	var back_plant_scene3 = load("res://world/plants/Plant_estetic3.tscn")

	var back_plant_amount = Global.estetic_back_plant_amount
	var back_plant_output = []
	var plants_x_pos = []
	var plants_to_close = false

	for _i in range(back_plant_amount):
		# Randomize which plant model is choosen
		rng.randomize()
		var rand = rng.randf()

		var scene = back_plant_scene1
		if rand < 0.3:
			scene = back_plant_scene2
		elif rand < 0.6:
			scene = back_plant_scene3
		
		# Instance and set up plant
		var plant_instance = scene.instance()
		var x_pos = rng.randi_range(20,world_width-20)
		var y_pos = rng.randi_range(bottom_height, int(abs_bottom_height*0.98))
		if plants_x_pos:
			plants_to_close = true
		while plants_to_close:
			plants_to_close = false
			for other_plant_x in plants_x_pos:
				if abs(other_plant_x - x_pos) < 170:
					plants_to_close = true
					x_pos = rng.randi_range(20, (world_width-20))
					continue

		plant_instance.position = Vector2(x_pos, y_pos)
		back_plant_output.append(plant_instance)
	
	# Return a list of plants
	return back_plant_output


func reset_global_values():
	world_size_mult = Global.world_size_screen_multiple
	world_width = screen_size.x*world_size_mult
	world_height = screen_size.y*world_size_mult

	# world variables
	bottom_height = world_height*(1-Global.world_floor_multiple)
	abs_bottom_height = world_height*(1-Global.world_floor_absolute_bottom)

