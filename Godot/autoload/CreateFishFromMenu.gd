extends Node

# Values collected from create fish menu
# and sent to new fish by "set_attribute"
var height = 10
var width = 10
var mass = 10
var eyes = 10
var fin = 10
var mouth = 10
var teeth = 10
var aggressive = 10
var nervous = 10
var hunger = 10
var passion = 10
var selfish = 10
var conforming = 10
var mutable = 10

var genders = ["female", "male"]


func set_attributes(fish):
    var rng = RandomNumberGenerator.new()
    rng.randomize()

    # Set physical attributes
    fish.width = width
    fish.height = height
    fish.eye_size = eyes
    fish.fin_size = fin
    fish.mouth_size = mouth
    fish.teeth_size = teeth

    # Set social attributes
    fish.gender = genders[rng.randi_range(0,1)]
    fish.aggressive = aggressive
    fish.nervous = nervous
    fish.hunger = hunger
    fish.passion = passion
    fish.selfish = selfish
    fish.conforming = conforming
    fish.mutable = mutable
