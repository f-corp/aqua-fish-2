extends Node


func set_behaviour(fish):
	""" Main method to set behaviour and behaviour timer """
	var rng = RandomNumberGenerator.new()
	var behaviour_timer = fish.get_node("behaviour_timer")
	
	""" Fish hard coded behaviours """
	if fish.health <= 0 or not fish.is_alive:
		fish.behaviour = "dead"
	
	if fish.behaviour == "dead":
		return
	
	if fish.behaviour == "acquire food":
		if fish.energy >= fish.energy_max*0.99 and fish.acquire_food_list():
			fish.reset_behaviour()
	
	if fish.behaviour == "acquire partner":
		if fish.chosen_partner == null or not fish.chosen_partner.is_alive:
			fish.reset_behaviour()
	
	if fish.behaviour == "hunt":
		if fish.chosen_prey == null or not fish.chosen_prey.is_alive:
			fish.reset_behaviour()

	# only run behaviour choice code if the fish needs new behaviour
	if not fish.needs_new_behaviour:
		return
	
	""" Fish behaviour choice """
	var possible_behaviours = []
	var base_chance = Global.fish_base_behaviour_chance
	
	# Add normal behaviours
	for _i in range(base_chance):
		possible_behaviours.append_array(["do nothing"])
		possible_behaviours.append_array(["random swim"])
	
	if not food_spotted(fish) and not found_possible_partner(fish):
		var chance = fish.nervous
		for _i in range(int(chance)):
			possible_behaviours.append_array(["random swim"])

	if can_shoal(fish):
		var chance = fish.conforming*0.8 + (100 - fish.selfish)*0.2
		for _i in range(int(chance)):
			possible_behaviours.append_array(["shoal"])

	if food_spotted(fish):
		var chance = fish.hunger + pow((fish.energy_max - fish.energy), 0.5)
		for _i in range(int(chance)):
			possible_behaviours.append_array(["acquire food"])
	
	if ready_to_kiss(fish):
		if found_possible_partner(fish):
			var chance = fish.passion*0.8 + fish.health*0.1 + fish.energy*0.1
			for _i in range(int(chance)):
				possible_behaviours.append_array(["acquire partner"])

	if ready_to_hunt(fish):
		if found_possible_prey(fish):
			var chance = fish.aggressive*0.6 + fish.hunger*0.2 + fish.teeth_size*0.2
			for _i in range(int(chance)):
				possible_behaviours.append_array(["hunt"])
	
	if is_attacked(fish):
		var chance_flee = fish.nervous + fish.selfish +\
						  pow((fish.health_max-fish.health),0.5)

		var chance_defend = fish.aggressive + fish.hunger +\
							pow(fish.health, 0.5)

		for _i in range(int(chance_flee)):
			possible_behaviours.append_array(["flee"])
		for _i in range(int(chance_defend)):
			possible_behaviours.append_array(["defend"])

	# Pick a random behaviour and return
	possible_behaviours.shuffle()
	fish.behaviour = possible_behaviours[0]
	fish.needs_new_behaviour = false
	
	if fish.behaviour == "do nothing":
		behaviour_timer.set_wait_time(rng.randf_range(1.5,3))
		behaviour_timer.start()
	elif fish.behaviour == "random swim":
		behaviour_timer.set_wait_time(rng.randf_range(1.5,3))
		behaviour_timer.start()
	elif fish.behaviour == "shoal":
		behaviour_timer.set_wait_time(rng.randf_range(3, 5))
		behaviour_timer.start()
	elif fish.behaviour == "acquire partner":
		behaviour_timer.set_wait_time(12)
		behaviour_timer.start()
	elif fish.behaviour == "hunt":
		behaviour_timer.set_wait_time(rng.randf_range(5,12))
		behaviour_timer.start()
	elif fish.behaviour == "flee":
		behaviour_timer.set_wait_time(3)
		behaviour_timer.start()
	elif fish.behaviour == "defend":
		behaviour_timer.set_wait_time(3)
		behaviour_timer.start()


func can_shoal(fish):
	if fish.conforming > Global.fish_conforming_shoal_cutoff:
		if fish.acquire_detected_kin_list().size() >= 2:
			return true
	else:
		return false


func food_spotted(fish):
	if fish.acquire_food_list() and fish.energy <= fish.energy_max*0.95:
		return true
	else:
		return false


func ready_to_kiss(fish):
	if fish.energy > fish.energy_max*Global.fish_energy_kiss_factor and \
		fish.health > fish.health_max*Global.fish_health_kiss_factor and\
		not fish.is_pregnant and not fish.is_a_fish_child:
		return true

	else:
		return false


func found_possible_partner(fish):
	if fish.detected_fishes:
		for other_fish in fish.detected_fishes:
			if FishSizeAttributesOutputs.is_fishes_kin(fish, other_fish) and\
				not other_fish.is_pregnant and other_fish.is_alive and not other_fish.is_a_fish_child and\
				not other_fish.is_menu_created and other_fish.is_active:
				return true
				
	return false


func ready_to_hunt(fish):
	if fish.energy < fish.energy_max*0.9 and\
		fish.aggressive >= Global.fish_aggressive_hunt_cutoff and\
		fish.teeth_size >= Global.fish_teeth_hunt_cutoff and\
		not fish.is_a_fish_child:
		return true
	
	return false


func found_possible_prey(fish):
	if fish.search_for_prey():
		return true
	return false


func is_attacked(fish):
	if fish.is_attacked:
		return true
	return false


func reset_behaviour(fish):
	fish.needs_new_behaviour = true
	fish.is_idle_swimming = false
	fish.is_eating = false
	fish.is_looking_at_food = false
	fish.is_hunting = false
	fish.is_attacked = false

	fish.behaviour = "do nothing"
