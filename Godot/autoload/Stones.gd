extends Node

# Load stones to spaws behind fishes
var stone_b1_scene = preload("res://world/stones/Stone_back1.tscn")
var stone_b2_scene = preload("res://world/stones/Stone_back2.tscn")
var stone_b3_scene = preload("res://world/stones/Stone_back3.tscn")

# Load stones to spawn in fron of fishes
var stone_f1_scene = preload("res://world/stones/Stone_front1.tscn")
var stone_f2_scene = preload("res://world/stones/Stone_front2.tscn")
var stone_f3_scene = preload("res://world/stones/Stone_front3.tscn")


# Load variables
var screen_size = OS.get_screen_size()
var rng = RandomNumberGenerator.new()

var world_size_mult = Global.world_size_screen_multiple
var world_width = screen_size.x*world_size_mult
var world_height = screen_size.y*world_size_mult

var bottom_height = world_height*(1-Global.world_floor_multiple)
var abs_bottom_height = world_height*(1-Global.world_floor_absolute_bottom)
var stone_amount = Global.world_stone_amount
var stone_back_proportion = Global.world_stone_back_proportion


func add_stones_back():
	var stones = []
	var stones_x_pos  = []
	var stones_to_close = false
	for _i in range(int(stone_amount*stone_back_proportion)):
		
		# randomize which stone is picked
		var stone_instance = stone_b1_scene.instance()
		var stone_picker = randf()
		if stone_picker < 0.33:
			stone_instance = stone_b2_scene.instance()
		elif stone_picker < 0.66:
			stone_instance = stone_b3_scene.instance()

		var y_pos = rng.randi_range(bottom_height, int(abs_bottom_height*0.98))
		var x_pos = rng.randi_range(20, (world_width-20))
		
		# test if stone is to close to other stone
		if stones_x_pos:
			stones_to_close = true
		while stones_to_close:
			stones_to_close = false
			for other_stones_x in stones_x_pos:
				if abs(other_stones_x - x_pos) < 170:
					stones_to_close = true
					x_pos = rng.randi_range(20, (world_width-20))
					continue
		
		# Create stone and add it to output list
		stone_instance.position = Vector2(x_pos, y_pos)
		stones_x_pos.append(x_pos)
		stones.append(stone_instance)
	return stones


func add_stones_front():
	var stones = []
	var stones_x_pos  = []
	var stones_to_close = false
	for _i in range(int(stone_amount*(1-stone_back_proportion))):
		
		# randomize which stone is picked
		var stone_instance = stone_f1_scene.instance()
		var stone_picker = randf()
		if stone_picker < 0.33:
			stone_instance = stone_f2_scene.instance()
		elif stone_picker < 0.66:
			stone_instance = stone_f3_scene.instance()

		var y_pos = rng.randi_range(abs_bottom_height, int(abs_bottom_height*1.04))
		var x_pos = rng.randi_range(20, (world_width-20))
		
		# test if stone is to close to other stone
		if stones_x_pos:
			stones_to_close = true
		while stones_to_close:
			stones_to_close = false
			for other_stones_x in stones_x_pos:
				if abs(other_stones_x - x_pos) < 120:
					stones_to_close = true
					x_pos = rng.randi_range(20, (world_width-20))
					continue
		
		# Create stone and add it to output list
		stone_instance.position = Vector2(x_pos, y_pos)
		stones_x_pos.append(x_pos)
		stones.append(stone_instance)
	return stones


func reset_global_values():
	world_size_mult = Global.world_size_screen_multiple
	world_width = screen_size.x*world_size_mult
	world_height = screen_size.y*world_size_mult

	bottom_height = world_height*(1-Global.world_floor_multiple)
	abs_bottom_height = world_height*(1-Global.world_floor_absolute_bottom)
