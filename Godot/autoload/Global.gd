extends Node

# System variables
var version = "1.0.1"
var simulation_time_scale = 1
var statistics_logging_on = true
var statistics_time_intervals = 10

# World variables
var world_size_screen_multiple = 4
var world_floor_multiple = 0.12
var world_floor_absolute_bottom = 0.06
var world_stone_amount = 20
var world_stone_back_proportion = 0.5

var gravity_acceleration = 9.82

# Fish variables
var start_fish_amount = 60
var fish_attribute_start_value_min = 8
var fish_attribute_start_value_max = 25
var fish_attribute_start_variance = 4
var fish_distance_start_variance = 200
var fish_start_group_size = 7

var fish_can_eat_distance = 20
var fish_eat_animation_time = 0.2
var fish_eat_energy_factor = 0.008
var fish_detection_multiplyer = 5
var fish_detect_attacker_multiplyer = 8
var fish_max_kin_square_difference = 500
var fish_energy_kiss_factor = 0.5
var fish_health_kiss_factor = 0.5

var fish_pregnant_time = 6
var fish_child_growth_time = 20
var fish_mutable_random_max = 1.2
var fish_mutable_random_min = 0.8

var fish_base_behaviour_chance = 10
var fish_conforming_shoal_cutoff = 22
var fish_aggressive_hunt_cutoff = 10
var fish_teeth_hunt_cutoff = 25

var fish_attack_multiplyer = 1
var fish_attack_energy_drain_multiplyer = 0.1

var fish_eat_berries_mouth_min = 0
var fish_eat_berries_mouth_max = 18
var fish_eat_berries_teeth_min = 0
var fish_eat_berries_teeth_max = 20
var fish_eat_carrots_teeth_min = 8
var fish_eat_carrots_teeth_max = 35
var fish_eat_fish_teeth_min = 22
var fish_eat_fish_teeth_max = 100
var fish_hunt_fish_teeth_min = 25
var fish_hunt_fish_teeth_max = 100

var fish_heal_per_second_percent = 0.5
var fish_energy_drain_multiplyer = 0.05
var fish_low_energy_damage = 0.5
var fish_low_energy_level = 0.1

# Food variables
var berry_plant_amount = 10
var berry_growth_timer_min = 10
var berry_growth_timer_max = 25
var berry_start_prob = 0.3
var berry_energy_value = 75.0

var carrot_water_res_const = 1
var carrot_mass = 20
var carrot_initial_speed = 50
var carrot_drop_timer_min = 1
var carrot_drop_timer_max = 5
var carrot_energy_value = 150.0

# World estetic variables
var estetic_front_plant_amount = 10
var estetic_back_plant_amount = 10

# Camera variables
var camera_mouse_border_speed = 10
var camera_button_speed = 10
var camera_zoom_speed = 0.07
var camera_zoom_min_value = 0.15

# UI variables
var menu_visable = false
var menu_fish_info_visable = false
var menu_is_mouse_inside_menues = false
var menu_is_mouse_inside_info_menues = false

var main_menu_fish_amount = 10

# Sound variables
var sound_is_off = false
var sound_master_volume = 70

# How to play varables
var htp_first_fish_pos = Vector2.ZERO
var htp_first_fish_vel = Vector2.ZERO
var htp_first_fish_scale = Vector2.ZERO
