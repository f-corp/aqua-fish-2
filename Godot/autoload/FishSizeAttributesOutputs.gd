extends Node


func set_outputs(fish):
	
	var rng = RandomNumberGenerator.new()
	rng.randomize()

	# Store variables to simplify code
	var width = fish.width
	var height = fish.height
	var eye_size = fish.eye_size
	var fin_size = fish.fin_size
	var mouth_size = fish.mouth_size
	var teeth_size = fish.teeth_size
	
	var mass = width*height

	# set all outputs
	fish.mass = mass
	fish.mass_movement = 10*pow(mass, 0.5) # Reduce mass movement reduction
	fish.fin_force = fin_size
	fish.water_resistance = height*0.001
	fish.health = mass*rng.randf_range(0.5, 1.0)
	fish.health_max = mass
	fish.energy = mass*rng.randf_range(0.5, 1.0)
	fish.energy_max = mass
	fish.energy_drain = (pow(mass, 0.5) + eye_size + fin_size + mouth_size + teeth_size*0.5) / 4.5
	fish.eat_speed = mouth_size
	fish.awareness = eye_size
	fish.attack = teeth_size
	fish.defence = teeth_size + height + pow(mass,0.5)
	fish.food_energy_value = mass
	fish.food_energy_value_max = mass

	fish.food_capability = []
	if teeth_size >= Global.fish_eat_berries_teeth_min and teeth_size <= Global.fish_eat_berries_teeth_max \
		and mouth_size >= Global.fish_eat_berries_mouth_min and mouth_size <= Global.fish_eat_berries_mouth_max:
		fish.food_capability.append("berries")
	if teeth_size >= Global.fish_eat_carrots_teeth_min and teeth_size <= Global.fish_eat_carrots_teeth_max:
		fish.food_capability.append("carrots")
	if teeth_size >= Global.fish_eat_fish_teeth_min and teeth_size <= Global.fish_eat_fish_teeth_max:
		fish.food_capability.append("fishes")


func update_size(fish):

	# Save body part nodes
	var body = fish.get_node("Textures/body")
	var fin_back = fish.get_node("Textures/fin_back")
	var fin_top = fish.get_node("Textures/fin_top")
	var fin_bot = fish.get_node("Textures/fin_bot")
	var eye_left = fish.get_node("Textures/eye_left")
	var eye_right = fish.get_node("Textures/eye_right")
	var mouth_top = fish.get_node("Textures/mouth_top")
	var mouth_bot = fish.get_node("Textures/mouth_bot")
	var teeth = fish.get_node("Textures/teeth")
	var mouth_inside = fish.get_node("Textures/mouth_inside")
	
	var health_bar
	var energy_bar
	var info_selected_arrow

	if not fish.is_in_main_menu:
		health_bar = fish.get_node("Sliders/HealthBar")
		energy_bar = fish.get_node("Sliders/EnergyBar")
		info_selected_arrow = fish.get_node("info_selected_arrow")

	# Store varibles to simplify code
	var width = fish.width
	var height = fish.height
	var eye_size = fish.eye_size
	var fin_size = fish.fin_size
	var mouth_size = fish.mouth_size
	var teeth_size = fish.teeth_size

	var health_max = fish.health_max
	var energy_max = fish.energy_max

	# Rescale and translate textures based on fish attributes
	body.scale.x = width*0.1
	body.scale.y = height*0.1
	fish.get_node("CollisionShape2D").scale = Vector2(width*0.16, height*0.1)
	
	eye_left.scale = 3.16*pow(eye_size, 0.5)*Vector2(0.1,0.1)
	eye_right.scale = 3.16*pow(eye_size, 0.5)*Vector2(0.1,0.1)
	eye_left.position.y = -6.6*height - 19
	eye_right.position.y = 1-6.6*height
	eye_left.position.x = -14.8*width
	eye_right.position.x = -14.8*width + 4.3*3.16*pow(eye_size,0.5)

	fin_back.scale.x = fin_size*0.055 + width*0.045
	fin_back.scale.y = height*0.1
	fin_back.position.x = width*19.5 + fin_size*1.5 - 4
	
	fin_top.scale.x = width*0.1
	fin_top.scale.y = fin_size*0.05 + height*0.05
	fin_top.position.y = -14.0*height - 0.7*fin_size

	fin_bot.scale.x = width*0.1
	fin_bot.scale.y = fin_size*0.02 + height*0.06 + 0.2
	fin_bot.position.x = 1.4*width + 20
	fin_bot.position.y = 14.0*height + 0*fin_size + 3

	mouth_top.scale = 3.16*pow(mouth_size, 0.5)*Vector2(0.1,0.1)
	mouth_top.position.x = -19.8*width + 40
	mouth_top.position.y = 2.5*height - 3.16*pow(mouth_size, 0.5) + 10
	mouth_bot.scale = 3.16*pow(mouth_size, 0.5)*Vector2(0.1,0.1)
	mouth_bot.position.x = -19.8*width + 43
	mouth_bot.position.y = 2.5*height + 3.16*pow(mouth_size, 0.5) + 19
	mouth_inside.scale = 3.16*pow(mouth_size, 0.5)*Vector2(0.1,0.08)
	mouth_inside.position.x = -19.8*width + 44
	mouth_inside.position.y = 2.5*height + 13

	teeth.scale.x = 3.16*pow(mouth_size, 0.5)*0.1
	teeth.scale.y = teeth_size*0.1
	teeth.position.x = -19.8*width + 38
	teeth.position.y = 2.5*height + 1.1*teeth_size + 12

	if not fish.is_in_main_menu:
		health_bar.rect_scale.x = 0.002*pow(health_max, 0.5)
		energy_bar.rect_scale.x = 0.002*pow(energy_max, 0.5)
		fish.get_node("Sliders").position.x = -0.4*width -0.3*height
		fish.get_node("Sliders").position.y = 1.3*height

		info_selected_arrow.position.y = 1.3*height + 27


func randomize_start_attributes(fish):
	# Randomize start attributes
	var rng = RandomNumberGenerator.new()
	rng.randomize()

	var attribute_start_min = Global.fish_attribute_start_value_min
	var attribute_start_max = Global.fish_attribute_start_value_max
	var genders = ["female", "male"]
	
	fish.height = rng.randi_range(attribute_start_min, attribute_start_max)
	fish.width = rng.randi_range(attribute_start_min, attribute_start_max)
	fish.mass = rng.randi_range(attribute_start_min, attribute_start_max)
	fish.eye_size = rng.randi_range(attribute_start_min, attribute_start_max)
	fish.fin_size = rng.randi_range(attribute_start_min, attribute_start_max)
	fish.mouth_size = rng.randi_range(attribute_start_min, attribute_start_max)
	fish.teeth_size = rng.randi_range(attribute_start_min, attribute_start_max)
	fish.gender = genders[rng.randi_range(0,1)]
	fish.aggressive = rng.randi_range(attribute_start_min, attribute_start_max)
	fish.hunger = rng.randi_range(attribute_start_min, attribute_start_max)
	fish.nervous = rng.randi_range(attribute_start_min, attribute_start_max)
	fish.selfish = rng.randi_range(attribute_start_min, attribute_start_max)
	fish.passion = rng.randi_range(attribute_start_min, attribute_start_max)
	fish.mutable = rng.randi_range(attribute_start_min, attribute_start_max)
	fish.conforming = rng.randi_range(attribute_start_min, attribute_start_max)


func set_attributes_from_parents(fish, mother, father):
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	
	var rng_min = Global.fish_mutable_random_min
	var rng_max = Global.fish_mutable_random_max

	# Mutable is mean of mother and fanther randomized
	# Mutable is a value 0 to 100. Divide by 10 and randomize Global mutable min/max on every attribute
	fish.mutable = (mother.mutable + father.mutable)*rng.randf_range(rng_min,rng_max) *0.5
	
	var genders = ["female", "male"]
	fish.gender = genders[rng.randi_range(0,1)]

	fish.height = (mother.height + father.height)*0.5 + 0.1*fish.mutable * rng.randf_range(-1,1)
	fish.width = (mother.width + father.width)*0.5 + 0.1*fish.mutable * rng.randf_range(-1,1)
	fish.mass = fish.height * fish.width
	fish.eye_size = (mother.eye_size + father.eye_size)*0.5 + 0.1*fish.mutable * rng.randf_range(-1,1)
	fish.fin_size = (mother.fin_size + father.fin_size)*0.5 + 0.1*fish.mutable * rng.randf_range(-1,1)
	fish.mouth_size = (mother.mouth_size + father.mouth_size)*0.5 + 0.1*fish.mutable * rng.randf_range(-1,1)
	fish.teeth_size = (mother.teeth_size + father.teeth_size)*0.5 + 0.1*fish.mutable * rng.randf_range(-1,1)
	
	fish.aggressive = (mother.aggressive + father.aggressive)*0.5 + 0.1*fish.mutable * rng.randf_range(-1,1)
	fish.hunger = (mother.hunger + father.hunger)*0.5 + 0.1*fish.mutable * rng.randf_range(-1,1)
	fish.nervous = (mother.nervous + father.nervous)*0.5 + 0.1*fish.mutable * rng.randf_range(-1,1)
	fish.selfish = (mother.selfish + father.selfish)*0.5 + 0.1*fish.mutable * rng.randf_range(-1,1)
	fish.passion = (mother.passion + father.passion)*0.5 + 0.1*fish.mutable * rng.randf_range(-1,1)
	fish.conforming = (mother.conforming + father.conforming)*0.5 + 0.1*fish.mutable * rng.randf_range(-1,1)

	# Make sure the attributes don't get lower than 1
	if fish.height < 2:
		fish.height = 2
		fish.mass = fish.height * fish.width
	if fish.width < 2:
		fish.width = 2
		fish.mass = fish.height * fish.width
	if fish.eye_size < 2:
		fish.eye_size = 2
	if fish.fin_size < 2:
		fish.fin_size = 2
	if fish.mouth_size < 2:
		fish.mouth_size = 2
	if fish.teeth_size < 2:
		fish.teeth_size = 2
	
	if fish.aggressive < 2:
		fish.aggressive = 2
	if fish.hunger < 2:
		fish.hunger = 2
	if fish.nervous < 2:
		fish.nervous = 2
	if fish.selfish < 2:
		fish.selfish = 2
	if fish.passion < 2:
		fish.passion = 2
	if fish.conforming < 2:
		fish.conforming = 2
	if fish.mutable < 2:
		fish.mutable = 2


func is_fishes_kin(asking_fish, fish2):
	var attribute_square_difference = \
	pow(asking_fish.height - fish2.height, 2) +\
	pow(asking_fish.width - fish2.width, 2) +\
	pow(asking_fish.eye_size - fish2.eye_size, 2) +\
	pow(asking_fish.fin_size - fish2.fin_size, 2) +\
	pow(asking_fish.mouth_size - fish2.mouth_size, 2) +\
	pow(asking_fish.teeth_size - fish2.teeth_size, 2) +\
	pow(asking_fish.aggressive - fish2.aggressive, 2) +\
	pow(asking_fish.hunger - fish2.hunger, 2) +\
	pow(asking_fish.nervous - fish2.nervous, 2) +\
	pow(asking_fish.selfish - fish2.selfish, 2) +\
	pow(asking_fish.passion - fish2.passion, 2) +\
	pow(asking_fish.mutable - fish2.mutable, 2) +\
	pow(asking_fish.conforming - fish2.conforming, 2)

	# selfish lowers kin recognition. Easier to eat other but harder to fins kiss partner
	var max_kin_square_difference = Global.fish_max_kin_square_difference + (10 - asking_fish.selfish)

	var is_kin = false
	if attribute_square_difference <= max_kin_square_difference:
		is_kin = true
	return is_kin


func set_body_fins_color(fish):
	# Save body part nodes
	var body = fish.get_node("Textures/body")
	var fin_back = fish.get_node("Textures/fin_back")
	var fin_top = fish.get_node("Textures/fin_top")
	var fin_bot = fish.get_node("Textures/fin_bot")

	var eye_left = fish.get_node("Textures/eye_left")
	var eye_right = fish.get_node("Textures/eye_right")
	var mouth_top = fish.get_node("Textures/mouth_top")
	var mouth_bot = fish.get_node("Textures/mouth_bot")

	var aggressive = fish.aggressive
	var nervous = fish.nervous
	var hunger = fish.hunger
	var selfish = fish.selfish
	var passion = fish.passion
	var conforming = fish.conforming
	var mutable = fish.mutable

	# Set colors based on personality
	var r_body = (12*pow(aggressive,0.5) + 12*pow(passion, 0.5)) /100
	var g_body = (6*pow(hunger,0.5) + 18*pow(nervous,0.5)) /100
	var b_body = (20*pow(conforming, 0.5) + 2*pow(passion,0.5)) /100 - 2*pow(selfish, 0.5)/100
	
	var g_fin = (4*pow(hunger,0.5) + 6*pow(nervous,0.5) + 8*pow(selfish, 0.5)) /100
	var r_fin = (12*pow(aggressive,0.5) + 6*pow(passion, 0.5)) /100
	var b_fin = (15*pow(conforming,0.5) + 3*pow(passion,0.5)) /100

	# Make eyes yellow gray with high mutable
	var r_eye = 1 - pow(mutable,2)/65000
	var g_eye = 1 - pow(mutable,2)/65000
	var b_eye = 1 - pow(mutable,2)/20000

	# Make mouth more red when aggressive is high
	var r_mouth = 1 - pow(aggressive, 2)/50000
	var g_mouth = 1 - pow(aggressive, 2)/20000
	var b_mouth = 1 - pow(aggressive, 2)/20000

	var damping = 0.77
	var color_body = Color(r_body*damping, g_body*damping, b_body*damping)
	var color_fins = Color(r_fin, g_fin, b_fin)
	var color_eye = Color(r_eye, g_eye, b_eye)
	var color_mouth = Color(r_mouth, g_mouth, b_mouth)

	body.self_modulate = color_body

	fin_back.self_modulate = color_fins
	fin_top.self_modulate = color_fins
	fin_bot.self_modulate = color_fins

	eye_left.self_modulate = color_eye
	eye_right.self_modulate = color_eye

	mouth_top.self_modulate = color_mouth
	mouth_bot.self_modulate = color_mouth
