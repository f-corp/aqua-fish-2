extends Node

# Save initital global values
var base_world_size = Global.world_size_screen_multiple
var base_carrot_drop_rate = Global.carrot_drop_timer_max
var base_simulation_speed = Global.simulation_time_scale
var base_statistics_on = Global.statistics_logging_on
var base_statistics_time = Global.statistics_time_intervals
var base_fish_start_amount = Global.start_fish_amount
var base_fish_attribute_min = Global.fish_attribute_start_value_min
var base_fish_attribute_max = Global.fish_attribute_start_value_max

# Sound variables
var base_sound_is_off = Global.sound_is_off
var base_sound_master_volume = Global.sound_master_volume
