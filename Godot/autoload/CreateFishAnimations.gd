extends Node


func create_all_animations(fish):
	create_animation_right_left_switch(fish)
	create_animation_death(fish)
	create_animation_idle_rotate_eyes(fish)
	create_animation_idle_rotate_mouth(fish)
	create_animation_mouth_attack(fish)
	create_animation_mouth_eat(fish)
	create_animation_mouth_kiss(fish)
	create_animation_fins(fish)


func create_animation_right_left_switch(fish):
	var left_right_switch = Animation.new()
	
	var anim_length = 0.2
	left_right_switch.set_length(anim_length)
	left_right_switch.loop = false
	
	# Create switch by scaling all texture from positive to negative
	var track_0 = left_right_switch.add_track(Animation.TYPE_VALUE)
	left_right_switch.track_set_path(track_0, "Textures:scale:x")
	left_right_switch.track_insert_key(track_0, 0, 0.08)
	left_right_switch.track_insert_key(track_0, anim_length, -0.08)

	fish.get_node("anim_left_right").add_animation("left_right_switch", left_right_switch)


func create_animation_death(fish):
	var death_changes = Animation.new()

	var anim_length = 1.0
	death_changes.set_length(anim_length)
	death_changes.loop = false

	# Body rotation (belly up)
	var track_0 = death_changes.add_track(Animation.TYPE_VALUE)
	var all_textures = fish.get_node("Textures").rotation_degrees
	death_changes.track_set_path(track_0, "Textures:rotation_degrees")
	death_changes.track_insert_key(track_0, 0, all_textures)
	death_changes.track_insert_key(track_0, anim_length, 180)

	# Left eye rotation
	var track_1 = death_changes.add_track(Animation.TYPE_VALUE)
	var eye_left_r0 = fish.get_node("Textures/eye_left").rotation_degrees
	death_changes.track_set_path(track_1, "Textures/eye_left:rotation_degrees")
	death_changes.track_insert_key(track_1, 0, eye_left_r0)
	death_changes.track_insert_key(track_1, anim_length, eye_left_r0 + 180)

	# Right eye rotation
	var track_2 = death_changes.add_track(Animation.TYPE_VALUE)
	var eye_right_r0 = fish.get_node("Textures/eye_right").rotation_degrees
	death_changes.track_set_path(track_2, "Textures/eye_right:rotation_degrees")
	death_changes.track_insert_key(track_2, 0, eye_right_r0)
	death_changes.track_insert_key(track_2, anim_length, eye_right_r0 - 180)

	fish.get_node("anim_death").add_animation("death_changes", death_changes)


func create_animation_idle_rotate_eyes(fish):
	var rotate_eyes = Animation.new()

	var anim_length = 1.2
	rotate_eyes.set_length(anim_length)
	rotate_eyes.loop = false

	# Left eye rotation
	var track_0 = rotate_eyes.add_track(Animation.TYPE_VALUE)
	var eye_left_r0 = fish.get_node("Textures/eye_left").rotation_degrees
	rotate_eyes.track_set_path(track_0, "Textures/eye_left:rotation_degrees")
	rotate_eyes.track_insert_key(track_0, 0, eye_left_r0)
	rotate_eyes.track_insert_key(track_0, anim_length*0.6, eye_left_r0 + 187)
	rotate_eyes.track_insert_key(track_0, anim_length, eye_left_r0)

	# Left eye rotation
	var track_1 = rotate_eyes.add_track(Animation.TYPE_VALUE)
	var eye_right_r0 = fish.get_node("Textures/eye_right").rotation_degrees
	rotate_eyes.track_set_path(track_1, "Textures/eye_right:rotation_degrees")
	rotate_eyes.track_insert_key(track_1, 0, eye_right_r0)
	rotate_eyes.track_insert_key(track_1, anim_length*0.7, eye_right_r0 - 223)
	rotate_eyes.track_insert_key(track_1, anim_length, eye_right_r0)

	fish.get_node("anim_idle").add_animation("rotate_eyes", rotate_eyes)


func create_animation_idle_rotate_mouth(fish):
	var rotate_mouth = Animation.new()

	var anim_length = 2.2
	rotate_mouth.set_length(anim_length)
	rotate_mouth.loop = false

	# Left eye rotation
	var track_0 = rotate_mouth.add_track(Animation.TYPE_VALUE)
	var mouth_bot_r0 = fish.get_node("Textures/mouth_bot").rotation_degrees
	rotate_mouth.track_set_path(track_0, "Textures/mouth_bot:rotation_degrees")
	rotate_mouth.track_insert_key(track_0, 0, mouth_bot_r0)
	rotate_mouth.track_insert_key(track_0, anim_length*0.2, mouth_bot_r0 - 5)
	rotate_mouth.track_insert_key(track_0, anim_length*0.4, mouth_bot_r0 + 5)
	rotate_mouth.track_insert_key(track_0, anim_length*0.6, mouth_bot_r0 - 5)
	rotate_mouth.track_insert_key(track_0, anim_length*0.8, mouth_bot_r0 + 5)
	rotate_mouth.track_insert_key(track_0, anim_length*1.0, mouth_bot_r0 - 5)
	rotate_mouth.track_insert_key(track_0, anim_length*1.2, mouth_bot_r0 + 5)
	rotate_mouth.track_insert_key(track_0, anim_length*1.4, mouth_bot_r0 - 5)
	rotate_mouth.track_insert_key(track_0, anim_length*1.6, mouth_bot_r0 + 5)
	rotate_mouth.track_insert_key(track_0, anim_length*1.8, mouth_bot_r0 - 5)
	rotate_mouth.track_insert_key(track_0, anim_length*2.0, mouth_bot_r0 + 5)
	rotate_mouth.track_insert_key(track_0, anim_length, mouth_bot_r0)

	fish.get_node("anim_idle").add_animation("rotate_mouth", rotate_mouth)


func create_animation_mouth_attack(fish):
	var mouth_attack = Animation.new()

	var anim_length = 0.5
	mouth_attack.set_length(anim_length)
	mouth_attack.loop = false

	# Top part of mouth rotation
	var track_0 = mouth_attack.add_track(Animation.TYPE_VALUE)
	var mouth_top_r0 = fish.get_node("Textures/mouth_bot").rotation
	mouth_attack.track_set_path(track_0, "Textures/mouth_top:rotation")
	mouth_attack.track_insert_key(track_0, 0, mouth_top_r0)
	mouth_attack.track_insert_key(track_0, 0.1, mouth_top_r0 + 0.1)
	mouth_attack.track_insert_key(track_0, anim_length, mouth_top_r0)

	# Teeth rotation
	var track_1 = mouth_attack.add_track(Animation.TYPE_VALUE)
	var teeth_r0 = fish.get_node("Textures/teeth").rotation
	mouth_attack.track_set_path(track_1, "Textures/teeth:rotation")
	mouth_attack.track_insert_key(track_1, 0, teeth_r0)
	mouth_attack.track_insert_key(track_1, 0.1, teeth_r0 + 0.1)
	mouth_attack.track_insert_key(track_1, anim_length, teeth_r0)

	# Bottom part of mouth rotation
	var track_2 = mouth_attack.add_track(Animation.TYPE_VALUE)
	var mouth_bot_r0 = fish.get_node("Textures/mouth_bot").rotation
	mouth_attack.track_set_path(track_2, "Textures/mouth_bot:rotation")
	mouth_attack.track_insert_key(track_2, 0, mouth_bot_r0)
	mouth_attack.track_insert_key(track_2, 0.1, mouth_bot_r0 - 0.1)
	mouth_attack.track_insert_key(track_2, anim_length, mouth_bot_r0)

	# Top part of mouth x-movement
	var track_3 = mouth_attack.add_track(Animation.TYPE_VALUE)
	var mouth_top_p0 = fish.get_node("Textures/mouth_top").position.x
	mouth_attack.track_set_path(track_3, "Textures/mouth_top:position:x")
	mouth_attack.track_insert_key(track_3, 0, mouth_top_p0)
	mouth_attack.track_insert_key(track_3, 0.1, mouth_top_p0*1.07)
	mouth_attack.track_insert_key(track_3, anim_length, mouth_top_p0)

	# Teeth x-movement
	var track_4 = mouth_attack.add_track(Animation.TYPE_VALUE)
	var teeth_px0 = fish.get_node("Textures/teeth").position.x
	mouth_attack.track_set_path(track_4, "Textures/teeth:position:x")
	mouth_attack.track_insert_key(track_4, 0, teeth_px0)
	mouth_attack.track_insert_key(track_4, 0.1, teeth_px0*1.07)
	mouth_attack.track_insert_key(track_4, anim_length, teeth_px0)

	# Bottom part of mouth x-movement
	var track_5 = mouth_attack.add_track(Animation.TYPE_VALUE)
	var mouth_bot_p0 = fish.get_node("Textures/mouth_bot").position.x
	mouth_attack.track_set_path(track_5, "Textures/mouth_bot:position:x")
	mouth_attack.track_insert_key(track_5, 0, mouth_bot_p0)
	mouth_attack.track_insert_key(track_5, 0.1, mouth_bot_p0*1.07)
	mouth_attack.track_insert_key(track_5, anim_length, mouth_bot_p0)

	# Inside part of mouth x-movement
	var track_6 = mouth_attack.add_track(Animation.TYPE_VALUE)
	var mouth_inside_p0 = fish.get_node("Textures/mouth_inside").position.x
	mouth_attack.track_set_path(track_6, "Textures/mouth_inside:position:x")
	mouth_attack.track_insert_key(track_6, 0, mouth_inside_p0)
	mouth_attack.track_insert_key(track_6, 0.1, mouth_inside_p0*1.07)
	mouth_attack.track_insert_key(track_6, anim_length, mouth_inside_p0)

	fish.get_node("anim_mouth").add_animation("mouth_attack", mouth_attack)


func create_animation_mouth_eat(fish):
	var eat_food = Animation.new()

	var anim_length = Global.fish_eat_animation_time
	eat_food.set_length(anim_length)
	eat_food.loop = false
	
	# Bottom part of mouth y-movement
	var track_0 = eat_food.add_track(Animation.TYPE_VALUE)
	var mouth_bot_p0 = fish.get_node("Textures/mouth_bot").position.y
	eat_food.track_set_path(track_0, "Textures/mouth_bot:position:y")
	eat_food.track_insert_key(track_0, 0, mouth_bot_p0)
	eat_food.track_insert_key(track_0, anim_length*0.5, mouth_bot_p0 + fish.mouth_size)
	eat_food.track_insert_key(track_0, anim_length, mouth_bot_p0)

	# Top part of mouth y-movement
	var track_1 = eat_food.add_track(Animation.TYPE_VALUE)
	var mouth_top_p0 = fish.get_node("Textures/mouth_top").position.y
	eat_food.track_set_path(track_1, "Textures/mouth_top:position:y")
	eat_food.track_insert_key(track_1, 0, mouth_top_p0)
	eat_food.track_insert_key(track_1, anim_length*0.5, mouth_top_p0 - fish.mouth_size)
	eat_food.track_insert_key(track_1, anim_length, mouth_top_p0)

	# Teeth y-movement
	var track_2 = eat_food.add_track(Animation.TYPE_VALUE)
	var teeth_p0 = fish.get_node("Textures/teeth").position.y
	eat_food.track_set_path(track_2, "Textures/teeth:position:y")
	eat_food.track_insert_key(track_2, 0, teeth_p0)
	eat_food.track_insert_key(track_2, anim_length*0.5, teeth_p0 - fish.mouth_size)
	eat_food.track_insert_key(track_2, anim_length, teeth_p0)

	# Mouth inside y-scale
	var track_3 = eat_food.add_track(Animation.TYPE_VALUE)
	var mouth_inside_s0 = fish.get_node("Textures/mouth_inside").scale.y
	eat_food.track_set_path(track_3, "Textures/mouth_inside:scale:y")
	eat_food.track_insert_key(track_3, 0, mouth_inside_s0)
	eat_food.track_insert_key(track_3, anim_length*0.5, mouth_inside_s0*2)
	eat_food.track_insert_key(track_3, anim_length, mouth_inside_s0)

	fish.get_node("anim_mouth").add_animation("eat_food", eat_food)
	

func create_animation_mouth_kiss(fish):
	var kiss = Animation.new()

	var anim_length = 2.0
	kiss.set_length(anim_length)
	kiss.loop = false
	
	# Top part of mouth x-movement
	var track_0 = kiss.add_track(Animation.TYPE_VALUE)
	var mouth_top_p0 = fish.get_node("Textures/mouth_top").position.x
	kiss.track_set_path(track_0, "Textures/mouth_top:position:x")
	kiss.track_insert_key(track_0, 0, mouth_top_p0)
	kiss.track_insert_key(track_0, anim_length*0.8, mouth_top_p0 - pow(fish.mouth_size,0.5)*3)
	kiss.track_insert_key(track_0, anim_length, mouth_top_p0)

	# Top part of mouth x-movement
	var track_1 = kiss.add_track(Animation.TYPE_VALUE)
	var mouth_bot_p0 = fish.get_node("Textures/mouth_bot").position.x
	kiss.track_set_path(track_1, "Textures/mouth_bot:position:x")
	kiss.track_insert_key(track_1, 0, mouth_bot_p0)
	kiss.track_insert_key(track_1, anim_length*0.8, mouth_bot_p0 - pow(fish.mouth_size,0.5)*3)
	kiss.track_insert_key(track_1, anim_length, mouth_bot_p0)

	# Teeth x-movement
	var track_2 = kiss.add_track(Animation.TYPE_VALUE)
	var teeth_p0 = fish.get_node("Textures/teeth").position.x
	kiss.track_set_path(track_2, "Textures/teeth:position:x")
	kiss.track_insert_key(track_2, 0, teeth_p0)
	kiss.track_insert_key(track_2, anim_length*0.8, teeth_p0 - pow(fish.mouth_size,0.5)*3)
	kiss.track_insert_key(track_2, anim_length, teeth_p0)

	# Mouth inside x-movement
	var track_3 = kiss.add_track(Animation.TYPE_VALUE)
	var mouth_inside_p0 = fish.get_node("Textures/mouth_inside").position.x
	kiss.track_set_path(track_3, "Textures/mouth_inside:position:x")
	kiss.track_insert_key(track_3, 0, mouth_inside_p0)
	kiss.track_insert_key(track_3, anim_length*0.8, mouth_inside_p0 - pow(fish.mouth_size,0.5)*3)
	kiss.track_insert_key(track_3, anim_length, mouth_inside_p0)

	# Left scale y-axis
	var track_4 = kiss.add_track(Animation.TYPE_VALUE)
	var eye_l_sy0 = fish.get_node("Textures/eye_left").scale.y
	kiss.track_set_path(track_4, "Textures/eye_left:scale:y")
	kiss.track_insert_key(track_4, 0, eye_l_sy0)
	kiss.track_insert_key(track_4, anim_length*0.2, eye_l_sy0*1.2)
	kiss.track_insert_key(track_4, anim_length*0.8, eye_l_sy0*1.2)
	kiss.track_insert_key(track_4, anim_length, eye_l_sy0)

	# right scale y-axis
	var track_5 = kiss.add_track(Animation.TYPE_VALUE)
	var eye_r_sy0 = fish.get_node("Textures/eye_right").scale.y
	kiss.track_set_path(track_5, "Textures/eye_right:scale:y")
	kiss.track_insert_key(track_5, 0, eye_r_sy0)
	kiss.track_insert_key(track_5, anim_length*0.2, eye_r_sy0*1.2)
	kiss.track_insert_key(track_5, anim_length*0.8, eye_r_sy0*1.2)
	kiss.track_insert_key(track_5, anim_length, eye_r_sy0)

	# Left scale x-axis
	var track_6 = kiss.add_track(Animation.TYPE_VALUE)
	var eye_l_sx0 = fish.get_node("Textures/eye_left").scale.x
	kiss.track_set_path(track_6, "Textures/eye_left:scale:x")
	kiss.track_insert_key(track_6, 0, eye_l_sx0)
	kiss.track_insert_key(track_6, anim_length*0.2, eye_l_sx0*1.2)
	kiss.track_insert_key(track_6, anim_length*0.8, eye_l_sx0*1.2)
	kiss.track_insert_key(track_6, anim_length, eye_l_sx0)

	# right scale x-axis
	var track_7 = kiss.add_track(Animation.TYPE_VALUE)
	var eye_r_sx0 = fish.get_node("Textures/eye_right").scale.x
	kiss.track_set_path(track_7, "Textures/eye_right:scale:x")
	kiss.track_insert_key(track_7, 0, eye_r_sx0)
	kiss.track_insert_key(track_7, anim_length*0.2, eye_r_sx0*1.2)
	kiss.track_insert_key(track_7, anim_length*0.8, eye_r_sx0*1.2)
	kiss.track_insert_key(track_7, anim_length, eye_r_sx0)

	fish.get_node("anim_mouth").add_animation("kiss", kiss)


func create_animation_fins(fish):
	var swim_normal = Animation.new()
	
	var anim_length = 0.3
	swim_normal.set_length(anim_length)
	swim_normal.loop = false
	
	# Back fin x-scale
	var track_0 = swim_normal.add_track(Animation.TYPE_VALUE)
	var fin_back_s0 = fish.get_node("Textures/fin_back").scale.x
	swim_normal.track_set_path(track_0, "Textures/fin_back:scale:x")
	swim_normal.track_insert_key(track_0, 0, fin_back_s0)
	swim_normal.track_insert_key(track_0, anim_length*0.5, fin_back_s0*1.2)
	swim_normal.track_insert_key(track_0, anim_length, fin_back_s0)
	
	# Back fin x-movement
	var track_1 = swim_normal.add_track(Animation.TYPE_VALUE)
	var fin_back_p0 = fish.get_node("Textures/fin_back").position.x
	swim_normal.track_set_path(track_1, "Textures/fin_back:position:x")
	swim_normal.track_insert_key(track_1, 0, fin_back_p0)
	swim_normal.track_insert_key(track_1, anim_length*0.5, fin_back_p0*1.02)
	swim_normal.track_insert_key(track_1, anim_length, fin_back_p0)

	# Bottom fin y-scale
	var track_2 = swim_normal.add_track(Animation.TYPE_VALUE)
	var fin_bot_s0 = fish.get_node("Textures/fin_bot").scale.y
	swim_normal.track_set_path(track_2, "Textures/fin_bot:scale:y")
	swim_normal.track_insert_key(track_2, 0, fin_bot_s0)
	swim_normal.track_insert_key(track_2, anim_length*0.5, fin_bot_s0*1.1)
	swim_normal.track_insert_key(track_2, anim_length, fin_bot_s0)

	# Bottom fin y-movement
	var track_3 = swim_normal.add_track(Animation.TYPE_VALUE)
	var fin_bot_p0 = fish.get_node("Textures/fin_bot").position.y
	swim_normal.track_set_path(track_3, "Textures/fin_bot:position:y")
	swim_normal.track_insert_key(track_3, 0, fin_bot_p0)
	swim_normal.track_insert_key(track_3, anim_length*0.5, fin_bot_p0*1.02)
	swim_normal.track_insert_key(track_3, anim_length, fin_bot_p0)

	fish.get_node("anim_fins").add_animation("swim_normal", swim_normal)
	
