extends Node


func create_fishes(main):
	
	var use_old_logic = false
	if use_old_logic:
		# create fishes
		for _i in range(Global.start_fish_amount):
			var rng = RandomNumberGenerator.new()
			var fish_instance = load("res://fish/Fish.tscn").instance()
			var x_pos = rng.randi_range(20,(main.world_width-20))
			var y_pos = rng.randi_range(20,(main.world_height-20))
			fish_instance.position = Vector2(x_pos, y_pos)
			fish_instance.randomize_fish_start_attributes()
			main.get_node("Fishes").add_child(fish_instance)
		return

	var rng = RandomNumberGenerator.new()
	rng.randomize()

	var fish_scene = load("res://fish/Fish.tscn")
	var start_amount = Global.start_fish_amount
	
	# make sure that fish attributes dont get to low 
	while Global.fish_attribute_start_value_min - Global.fish_attribute_start_variance <= 3:
		Global.fish_attribute_start_value_min += 0.1

	var attribute_start_min = Global.fish_attribute_start_value_min
	var attribute_start_max = Global.fish_attribute_start_value_max
	var attribute_variance = Global.fish_attribute_start_variance
	var distance_variance = Global.fish_distance_start_variance
	var group_size = Global.fish_start_group_size

	var x_pos = 0
	var y_pos = 0

	var height = 0
	var width = 0
	var mass = 0
	var eye_size = 0
	var fin_size = 0
	var mouth_size = 0
	var teeth_size = 0
	var genders = ["female", "male"]
	var aggressive = 0
	var hunger = 0
	var nervous = 0
	var selfish = 0
	var passion = 0
	var mutable = 0
	var conforming = 0

	for i in range(start_amount):
		if i%group_size == 0:
			x_pos = rng.randi_range(20,(main.world_width-20))
			y_pos = rng.randi_range(20,(main.world_height-20))
			
			height = rng.randi_range(attribute_start_min, attribute_start_max)
			width = rng.randi_range(attribute_start_min, attribute_start_max)
			mass = rng.randi_range(attribute_start_min, attribute_start_max)
			eye_size = rng.randi_range(attribute_start_min, attribute_start_max)
			fin_size = rng.randi_range(attribute_start_min, attribute_start_max)
			mouth_size = rng.randi_range(attribute_start_min, attribute_start_max)
			teeth_size = rng.randi_range(attribute_start_min, attribute_start_max)
			aggressive = rng.randi_range(attribute_start_min, attribute_start_max)
			hunger = rng.randi_range(attribute_start_min, attribute_start_max)
			nervous = rng.randi_range(attribute_start_min, attribute_start_max)
			selfish = rng.randi_range(attribute_start_min, attribute_start_max)
			passion = rng.randi_range(attribute_start_min, attribute_start_max)
			mutable = rng.randi_range(attribute_start_min, attribute_start_max)
			conforming = rng.randi_range(attribute_start_min, attribute_start_max)

		var fish = fish_scene.instance()

		var fish_x_pos = x_pos + distance_variance*rng.randf_range(-1,1)
		var fish_y_pos = y_pos + distance_variance*rng.randf_range(-1,1)
		fish.position = Vector2(int(fish_x_pos), int(fish_y_pos))

		fish.height = height + attribute_variance*rng.randf_range(-1,1)
		fish.width = width + attribute_variance*rng.randf_range(-1,1)
		fish.mass = mass + attribute_variance*rng.randf_range(-1,1)
		fish.eye_size = eye_size + attribute_variance*rng.randf_range(-1,1)
		fish.fin_size = fin_size + attribute_variance*rng.randf_range(-1,1)
		fish.mouth_size = mouth_size + attribute_variance*rng.randf_range(-1,1)
		fish.teeth_size = teeth_size + attribute_variance*rng.randf_range(-1,1)

		fish.gender = genders[rng.randi_range(0,1)]
		
		fish.aggressive = aggressive + attribute_variance*rng.randf_range(-1,1)
		fish.hunger = hunger + attribute_variance*rng.randf_range(-1,1)
		fish.nervous = nervous + attribute_variance*rng.randf_range(-1,1)
		fish.selfish = selfish + attribute_variance*rng.randf_range(-1,1)
		fish.passion = passion + attribute_variance*rng.randf_range(-1,1)
		fish.mutable = mutable + attribute_variance*rng.randf_range(-1,1)
		fish.conforming = conforming + attribute_variance*rng.randf_range(-1,1)
			
		main.get_node("Fishes").add_child(fish)
