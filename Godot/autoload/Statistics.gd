extends Node


var long_empty_array = [null]
var long_array_length = 1000
var time = 0

var active_means_array = []
var active_means_name = ""

# Attributes statiscics
var total_fishes = []
var height_means = []
var width_means = []
var mass_means = []
var eye_size_means = []
var fin_size_means = []
var mouth_size_means = []
var teeth_size_means = []

var gender_sums = []

var aggressive_means = []
var nervous_means = []
var hunger_means = []
var passion_means = []
var selfish_means = []
var conforming_means = []
var mutable_means = []


func _ready():
	long_empty_array.resize(long_array_length)
	make_arrays_longer()


func save_mean_values_to_list(fishes):
	if not Global.statistics_logging_on:
		time += 1
		return
	
	var height_sum = 0
	var width_sum = 0
	var mass_sum = 0
	var eye_size_sum = 0
	var fin_size_sum = 0
	var mouth_size_sum = 0
	var teeth_size_sum = 0
	
	var gender_array = []

	var aggressive_sum = 0
	var nervous_sum = 0
	var hunger_sum = 0
	var passion_sum = 0
	var selfish_sum = 0
	var conforming_sum = 0
	var mutable_sum = 0

	var fish_count = float(fishes.get_children().size())

	for fish in fishes.get_children():
		if fish.is_alive and fish.is_active and not fish.is_menu_created:
			height_sum += fish.height
			width_sum += fish.width
			mass_sum += fish.mass
			eye_size_sum += fish.eye_size
			fin_size_sum += fish.fin_size
			mouth_size_sum += fish.mouth_size
			teeth_size_sum += fish.teeth_size
			
			gender_array.append(fish.gender)

			aggressive_sum += fish.aggressive
			nervous_sum += fish.nervous
			hunger_sum += fish.hunger
			passion_sum += fish.passion
			selfish_sum += fish.selfish
			conforming_sum += fish.conforming
			mutable_sum += fish.mutable
	
	total_fishes[time] = fish_count
	height_means[time] = height_sum / fish_count
	width_means[time] = width_sum / fish_count
	mass_means[time] = mass_sum / fish_count
	eye_size_means[time] = eye_size_sum / fish_count
	fin_size_means[time] = fin_size_sum / fish_count
	mouth_size_means[time] = mouth_size_sum / fish_count
	teeth_size_means[time] = teeth_size_sum / fish_count
	
	gender_sums[time] = [gender_array.count("female"), gender_array.count("male")]
	
	aggressive_means[time] = aggressive_sum / fish_count
	nervous_means[time] = nervous_sum / fish_count
	hunger_means[time] = hunger_sum / fish_count
	passion_means[time] = passion_sum / fish_count
	selfish_means[time] = selfish_sum / fish_count
	conforming_means[time] = conforming_sum / fish_count
	mutable_means[time] = mutable_sum / fish_count
	
	# Increase array length when it gets full
	if time >= height_means.size()-1:
		make_arrays_longer()
	
	# Time i measured in second because statistics timer in main is set to 1
	time += 1
	# print(height_means.slice(0,time))
	# print_arrays()


func print_arrays():
	print(total_fishes)
	print(height_means)
	print(width_means)
	print(mass_means)
	print(eye_size_means)
	print(fin_size_means)
	print(mouth_size_means)
	print(teeth_size_means)

	print(gender_sums)

	print(aggressive_means)
	print(nervous_means)
	print(hunger_means)
	print(passion_means)
	print(selfish_means)
	print(conforming_means)
	print(mutable_means)


func make_arrays_longer():
	total_fishes.append_array(long_empty_array.duplicate(true))
	height_means.append_array(long_empty_array.duplicate(true))
	width_means.append_array(long_empty_array.duplicate(true))
	mass_means.append_array(long_empty_array.duplicate(true))
	eye_size_means.append_array(long_empty_array.duplicate(true))
	fin_size_means.append_array(long_empty_array.duplicate(true))
	mouth_size_means.append_array(long_empty_array.duplicate(true))
	teeth_size_means.append_array(long_empty_array.duplicate(true))

	gender_sums.append_array(long_empty_array.duplicate(true))

	aggressive_means.append_array(long_empty_array.duplicate(true))
	nervous_means.append_array(long_empty_array.duplicate(true))
	hunger_means.append_array(long_empty_array.duplicate(true))
	passion_means.append_array(long_empty_array.duplicate(true))
	selfish_means.append_array(long_empty_array.duplicate(true))
	conforming_means.append_array(long_empty_array.duplicate(true))
	mutable_means.append_array(long_empty_array.duplicate(true))


func reset_global_values():
	time = 0
	total_fishes = []
	height_means = []
	width_means = []
	mass_means = []
	eye_size_means = []
	fin_size_means = []
	mouth_size_means = []
	teeth_size_means = []

	gender_sums = []

	aggressive_means = []
	nervous_means = []
	hunger_means = []
	passion_means = []
	selfish_means = []
	conforming_means = []
	mutable_means = []

	make_arrays_longer()

