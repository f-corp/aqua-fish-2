extends Node

# system variables
var screen_size = OS.get_screen_size()
var world_width = screen_size.x*Global.world_size_screen_multiple
var world_height = screen_size.y*Global.world_size_screen_multiple
var ws = Global.world_size_screen_multiple

var speed_mouse = Global.camera_mouse_border_speed
var speed_button = Global.camera_button_speed
var zoom_speed = Global.camera_zoom_speed
var zoom_min = Global.camera_zoom_min_value

# Camera drag
var mouse_start_pos
var screen_start_position
var dragging = false


# ---------- Camera button control ----------
func check_move_and_zoom_controls(cam, event):
	scale_menues(cam)

	# Zoom
	var mouse_position = Vector2(0,0)
	if event is InputEventMouse:
		mouse_position = event.position
	if event.is_action("zoom_in"):
		camera_zoom_in(cam, mouse_position)
	if event.is_action("zoom_out"):
		camera_zoom_out(cam)
	
	# Camera movement
	if event.is_action("ui_left"):
		camera_move_left(cam, speed_button)
	if event.is_action("ui_right"):
		camera_move_right(cam, speed_button)
	if event.is_action("ui_up"):
		camera_move_up(cam, speed_button)
	if event.is_action("ui_down"):
		camera_move_down(cam, speed_button)
	# Camera drag
	if event.is_action("camera_mouse_drag"):
		camera_mouse_drag_on_off(cam, event)
	if event is InputEventMouseMotion and dragging:
		camera_mouse_drag(cam, event)


# ---------- Camera mouse border control ----------
func move_camera_if_mouse_at_border(cam, mouse_pos):
	# Move camera when mouse at edges
	var cam_pos = cam.get_camera_position()

	if mouse_pos.x <= (cam_pos.x - world_width/(ws*2) * cam.zoom.x + 10):
		camera_move_left(cam, speed_mouse)
	if mouse_pos.x >= (cam_pos.x + world_width/(ws*2) * cam.zoom.x - 10):
		camera_move_right(cam, speed_mouse)
	if mouse_pos.y <= (cam_pos.y - world_height/(ws*2) * cam.zoom.y + 10):
		camera_move_up(cam, speed_mouse)
	if mouse_pos.y >= (cam_pos.y + world_height/(ws*2)* cam.zoom.y - 10):
		camera_move_down(cam, speed_mouse)



# ---------- Auxiliary functions ----------
func camera_move_left(cam, speed):
	var cam_new_pos = cam.position + Vector2(-speed,0)*cam.zoom.x
	if (cam_new_pos.x - world_width/(ws*2) * cam.zoom.x) > 0:
		cam.position = cam_new_pos


func camera_move_right(cam, speed):
	var cam_new_pos = cam.position + Vector2(speed,0)*cam.zoom.x
	if (cam_new_pos.x + world_width/(ws*2) * cam.zoom.x) < world_width:
		cam.position = cam_new_pos	

func camera_move_up(cam, speed):
	var cam_new_pos = cam.position + Vector2(0,-speed)*cam.zoom.y
	if (cam_new_pos.y - world_height/(ws*2) * cam.zoom.y) > 0:
		cam.position = cam_new_pos

func camera_move_down(cam, speed):
	var cam_new_pos = cam.position + Vector2(0,speed)*cam.zoom.y
	if (cam_new_pos.y + world_height/(ws*2) * cam.zoom.y) < world_height:
		cam.position = cam_new_pos


func camera_zoom_in(cam, mouse_pos):
	if Global.menu_is_mouse_inside_menues:
		return
	
	if cam.zoom.x < Global.camera_zoom_min_value:
		return

	var c0 = cam.global_position # camera position
	var v0 = cam.get_viewport().size # vieport size
	var c1 # next camera position
	var z0 = cam.zoom # current zoom value
	var z1 = z0 * (1-Global.camera_zoom_speed) # next zoom value

	c1 = c0 + (-0.5*v0 + mouse_pos)*(z0 - z1)
	cam.zoom = z1
	cam.global_position = c1
	
	scale_volume(cam.zoom.x)
	scale_menues(cam)


func camera_zoom_out(cam):
	if Global.menu_is_mouse_inside_menues:
		return

	var cam_pos = cam.get_camera_position()

	var cam_new_zoom = cam.zoom * (1 + Global.camera_zoom_speed/cam.zoom.x)

	var left_ok = (cam_pos.x - world_width/(ws*2) * cam_new_zoom.x) > 0
	var right_ok = (cam_pos.x + world_width/(ws*2) * cam_new_zoom.x) < world_width
	var up_ok = (cam_pos.y - world_height/(ws*2) * cam_new_zoom.y) > 0
	var down_ok = (cam_pos.y + world_height/(ws*2) * cam_new_zoom.y) < world_height

	if right_ok and not left_ok:
		camera_move_right(cam, speed_mouse)
	elif left_ok and not right_ok:
		camera_move_left(cam, speed_mouse)
	
	if up_ok and not down_ok:
		camera_move_up(cam, speed_mouse)
	elif down_ok and not up_ok:
		camera_move_down(cam, speed_mouse)

	if left_ok and right_ok and up_ok and down_ok:
		if cam_new_zoom.x < ws*0.95:
			cam.zoom = cam_new_zoom
			scale_volume(cam.zoom.x)	
			scale_menues(cam)


func camera_mouse_drag_on_off(cam, event):
	if event.is_pressed():
		mouse_start_pos = event.position
		screen_start_position = cam.position
		dragging = true
	else:
		dragging = false

func camera_mouse_drag(cam, event):	
	var cam_old_pos = cam.get_camera_position()
	var previous_x = cam_old_pos.x
	var previous_y = cam_old_pos.y

	# move camera based on mouse movement
	cam.position = cam.zoom * (mouse_start_pos - event.position) + screen_start_position
	var cam_pos = cam.position

	# check if camera at borders
	var left_ok = (cam_pos.x - world_width/(ws*2) * cam.zoom.x) > 0
	var right_ok = (cam_pos.x + world_width/(ws*2) * cam.zoom.x) < world_width
	var up_ok = (cam_pos.y - world_height/(ws*2) * cam.zoom.y) > 0
	var down_ok = (cam_pos.y + world_height/(ws*2) * cam.zoom.y) < world_height

	# Stop camera if at borders
	if not left_ok:
		cam.position.x = max(cam.position.x, previous_x)
	elif not right_ok:
		cam.position.x = min(cam.position.x, previous_x)
	if not up_ok:
		cam.position.y = max(cam.position.y, previous_y)
	elif not down_ok:
		cam.position.y = min(cam.position.y, previous_y)


func scale_menues(cam):
	cam.get_node("Menus").set_scale(Vector2(1,1)*cam.zoom.x)
	cam.get_node("Text").set_scale(Vector2(1,1)*cam.zoom.x)


func scale_volume(zoom):
	var volume = -10*zoom +  3.4
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("FishSounds"), volume)


func reset_global_values():
	screen_size = OS.get_screen_size()
	world_width = screen_size.x*Global.world_size_screen_multiple
	world_height = screen_size.y*Global.world_size_screen_multiple
	ws = Global.world_size_screen_multiple

