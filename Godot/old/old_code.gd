

func set_behaviour_old(fish):
	""" Main method to set behaviour and behaviour timer """
	var behaviour = "do nothing"

	var rng = RandomNumberGenerator.new()
	var behaviour_timer = fish.get_node("behaviour_timer")

	if fish.needs_new_behaviour:
		if rng.randf() < 0.5:
			behaviour = "no nothing"
			behaviour_timer.set_wait_time(5*rng.randf())
			behaviour_timer.start()
		else:
			behaviour = "random swim"
			behaviour_timer.set_wait_time(5*rng.randf())
			behaviour_timer.start()
		
	# Set high so it does'nt interfere with other behaviour
	if fish.conforming >= 50:
		var kin_list = fish.acquire_detected_kin_list()
		if kin_list.size() >= 2:
			behaviour = "shoal"
			behaviour_timer.set_wait_time(5*rng.randf())
			behaviour_timer.start()
		
	# Allways on now if food is visalbe
	if fish.acquire_food_list():
		behaviour = "acquire food"
	elif fish.is_looking_at_food or fish.is_eating:
		fish.reset_behaviour()

	# Passion overrides other behaviours for now
	if fish.energy > fish.energy_max*Global.fish_energy_kiss_factor and \
		fish.health > fish.health_max*Global.fish_health_kiss_factor and\
		not fish.is_pregnant and not fish.is_a_fish_child:
		if fish.detected_fishes:
			for other_fish in fish.detected_fishes:
				if FishSizeAttributesOutputs.is_fishes_kin(fish, other_fish):
					behaviour = "acquire partner"
					fish.needs_new_behaviour = false
					continue
	
	if fish.health <= 0:
		behaviour = "dead"

	return behaviour