extends Node2D


onready var black_top = $Letterbox/Top
onready var black_bot = $Letterbox/Bot

var screen_size = OS.get_screen_size()


func _ready():
	screen_background_setup()

	# Placement and setup for buttons
	$ButtonNext.rect_position = Vector2(screen_size.x*0.5, screen_size.y*0.95 - 45)
	$ButtonPrevious.rect_position = Vector2(screen_size.x*0.5 - 220, screen_size.y*0.95 - 45)
	$ButtonToMenu.rect_position = Vector2(20, screen_size.y*0.95 - 45)

	$ButtonNext.visible = true
	$ButtonPrevious.visible = false
	$ButtonToMenu.visible = true

	# Set screen shot size and position
	$aquafish_screen_shot.position = screen_size*0.5
	$aquafish_screen_shot.scale = Vector2(1,1)*screen_size.x / 1920 * 0.4
	
	# Create first fish
	var fish_instance = load("res://fish/Fish_htp.tscn").instance()
	var base_fish_scale = 3
	fish_instance.scale = Vector2(base_fish_scale, base_fish_scale)
	fish_instance.position = Vector2(-90, screen_size.y*0.5)
	fish_instance.fish_name = "jens0"
	fish_instance.aggressive = 40
	fish_instance.conforming = 40
	$Fishes.add_child(fish_instance)

	# Set first text messages
	$Texts/first_label.rect_position = Vector2(screen_size.x*0.5-500, screen_size.y*0.15)
	$AnimationPlayer_text.play("text_animations")
	
	$Texts/first_list.rect_position = Vector2(screen_size.x*0.75,screen_size.y*0.4)
	$Texts/first_list_lines.rect_position = Vector2(screen_size.x*0.74,screen_size.y*0.4)


func _process(_delta):
	for fish in $Fishes.get_children():
		if fish.fish_name == "jens0" and fish.position.x > screen_size.x *0.1:
			fish.force_from_fins = Vector2.ZERO
			if fish.vel.x < 10:
				fish.vel.x = 0
		Global.htp_first_fish_pos = fish.global_position
		Global.htp_first_fish_vel = fish.vel
		Global.htp_first_fish_scale = fish.scale


func screen_background_setup(): 
	OS.window_fullscreen = true
	$FadeRectFull.visible = false
	$FadeRectInner.visible = false

	# Set letterbox size and position
	black_top.rect_size.x = screen_size.x
	black_top.rect_size.y = int(screen_size.y*0.1)
	
	black_bot.rect_size.x = screen_size.x
	black_bot.rect_size.y = int(screen_size.y*0.1)

	black_top.rect_position = Vector2(0, 0 - int(screen_size.y*0.2))
	black_bot.rect_position = Vector2(0, int(screen_size.y*1.1))
	
	# Fix to screen size bug
	$Background.rect_scale = Vector2(screen_size.x/1920, screen_size.y/1080)
	black_top.rect_scale = Vector2(screen_size.x/1920, screen_size.y/1080)
	black_bot.rect_scale = Vector2(screen_size.x/1920, screen_size.y/1080)
	
	create_letterbox_animation()
	$AnimationPlayer_background.play("letterbox_in")


func create_letterbox_animation():
	var letterbox_in = Animation.new()

	var anim_length = 2.5
	letterbox_in.set_length(anim_length)
	letterbox_in.loop = false

	# top box
	var track_0 = letterbox_in.add_track(Animation.TYPE_VALUE)
	var top_py0 = black_top.rect_position.y
	letterbox_in.track_set_path(track_0, "Letterbox/Top:rect_position:y")
	letterbox_in.track_insert_key(track_0, 0, top_py0)
	letterbox_in.track_insert_key(track_0, anim_length, top_py0 + int(screen_size.y*0.2))

	# bot box
	var track_1 = letterbox_in.add_track(Animation.TYPE_VALUE)
	var bot_py0 = black_bot.rect_position.y
	letterbox_in.track_set_path(track_1, "Letterbox/Bot:rect_position:y")
	letterbox_in.track_insert_key(track_1, 0, bot_py0)
	letterbox_in.track_insert_key(track_1, anim_length, bot_py0 - int(screen_size.y*0.2))

	$AnimationPlayer_background.add_animation("letterbox_in", letterbox_in)


func _on_ButtonNext_pressed():
	$AnimationPlayer_fade.play("NextFade")
	

func _on_ButtonToMenu_pressed():
	$FadeRect.visible = true
	$AnimationPlayer_fade.play("ToMenuFade")


func _on_AnimationPlayer_fade_animation_finished(anim_name):
	if anim_name == "ToMenuFade":
		var _scene = get_tree().change_scene("res://Main_menu/Main_menu.tscn")
	if anim_name == "NextFade":
		var _scene = get_tree().change_scene("res://how_to_play/how_to_play_1.tscn")


func _on_AnimationPlayer_background_animation_finished(anim_name):
	if anim_name == "letterbox_in":
		$AnimationPlayer_background.play("screen_shot_in")

