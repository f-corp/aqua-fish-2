extends Node2D


onready var black_top = $Letterbox/Top
onready var black_bot = $Letterbox/Bot

var screen_size = OS.get_screen_size()


func _ready():
	screen_background_setup()

	# Placement and setup for buttons
	$ButtonNext.rect_position = Vector2(screen_size.x*0.5, screen_size.y*0.95 - 45)
	$ButtonPrevious.rect_position = Vector2(screen_size.x*0.5 - 220, screen_size.y*0.95 - 45)
	$ButtonToMenu.rect_position = Vector2(20, screen_size.y*0.95 - 45)

	$ButtonNext.visible = true
	$ButtonPrevious.visible = true
	$ButtonToMenu.visible = true

	$ComputerMouse.position = Vector2(screen_size.x*0.5, screen_size.y*0.55)
	$Texts/Scroll.position = Vector2(screen_size.x*0.5-90, screen_size.y*0.55-40)
	$Texts/LeftClick.position = Vector2(screen_size.x*0.5-150, screen_size.y*0.55)
	$Texts/TryDrag.rect_position = Vector2(screen_size.x*0.7, screen_size.y*0.45)
	
	# Create first fish
	var fish_instance = load("res://fish/Fish_htp.tscn").instance()
	fish_instance.scale = Global.htp_first_fish_scale
	fish_instance.get_node("Textures").scale.x = -0.08
	fish_instance.position = Global.htp_first_fish_pos
	if fish_instance.position.x < screen_size.x*0.1:
		fish_instance.position.x = screen_size.x*0.1
	fish_instance.vel = Global.htp_first_fish_vel
	fish_instance.fish_name = "jens1"
	fish_instance.aggressive = 40
	fish_instance.conforming = 40
	$Fishes.add_child(fish_instance)

	# Set first text messages
	$Texts/first_label.rect_position = Vector2(screen_size.x*0.5-500, screen_size.y*0.15)
	$AnimationPlayer_text.play("text_animations")
	

func _process(_delta):
	pass


func screen_background_setup(): 
	OS.window_fullscreen = true
	$FadeRectFull.visible = false
	$FadeRectInner.visible = false

	# Set letterbox size and position
	black_top.rect_size.x = screen_size.x
	black_top.rect_size.y = int(screen_size.y*0.1)
	
	black_bot.rect_size.x = screen_size.x
	black_bot.rect_size.y = int(screen_size.y*0.1)

	black_top.rect_position = Vector2(0, 0)
	black_bot.rect_position = Vector2(0, int(screen_size.y*0.9))
	
	# Fix to screen size bug
	$Background.rect_scale = Vector2(screen_size.x/1920, screen_size.y/1080)
	black_top.rect_scale = Vector2(screen_size.x/1920, screen_size.y/1080)
	black_bot.rect_scale = Vector2(screen_size.x/1920, screen_size.y/1080)
	

func _on_ButtonNext_pressed():
	$AnimationPlayer_fade.play("NextFade")


func _on_ButtonPrevious_pressed():
	$AnimationPlayer_fade.play("PreviousFade")
	

func _on_ButtonToMenu_pressed():
	$FadeRectFull.visible = true
	$AnimationPlayer_fade.play("ToMenuFade")


func _on_AnimationPlayer_fade_animation_finished(anim_name):
	if anim_name == "ToMenuFade":
		var _scene = get_tree().change_scene("res://Main_menu/Main_menu.tscn")
	if anim_name == "NextFade":
		print("next")
	if anim_name == "PreviousFade":
		var _scene = get_tree().change_scene("res://how_to_play/how_to_play_base.tscn")

