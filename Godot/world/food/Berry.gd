extends Node2D

var mass = 10
var vel = Vector2(0,0)
var energy = Global.berry_energy_value
var max_energy = Global.berry_energy_value


func _input(_event):
	pass


func _ready():
	pass


func _process(_delta):
	if energy <= 0:
		get_parent().berry_removed()
		queue_free()
	elif energy <= max_energy*0.25:
		$AnimatedSprite.set_frame(3)
	elif energy <= max_energy*0.5:
		$AnimatedSprite.set_frame(2)
	elif energy <= max_energy*0.75:
		$AnimatedSprite.set_frame(1)


func remove_berry():
	get_parent().berry_removed()
	queue_free()

func gets_eaten(bite_size):
	energy -= bite_size
