extends KinematicBody2D

var water_resistance = Global.carrot_water_res_const
var mass = Global.carrot_mass
var energy = Global.carrot_energy_value
var max_energy = Global.carrot_energy_value

var force = Vector2()
var acc = Vector2()
var vel = Vector2()
var v_0 = Global.carrot_initial_speed
var pos_previous = Vector2()

var last_delta : float

var screen_size = OS.get_screen_size()
var world_height = screen_size.y*Global.world_size_screen_multiple
var world_width = screen_size.x*Global.world_size_screen_multiple
var bottom_height = world_height*(1-Global.world_floor_absolute_bottom)
var bottom_stop
var at_bottom = false

var is_mouse_on_carrot = false
var is_mouse_dragged = false

var g = Global.gravity_acceleration
var rng = RandomNumberGenerator.new()

func _input(event):
	""" All inputs """
	if event.is_action_pressed("left_click") and is_mouse_on_carrot:
		is_mouse_dragged = true

	if event.is_action_released("left_click") and is_mouse_dragged:
		is_mouse_dragged = false
		
		# v = s/t (duh)
		vel = (position - pos_previous)/last_delta
		# Make sure velocity does'nt get out of hand
		if vel.x > 1200:
			vel.x = 1200
		if vel.x < -1200:
			vel.x = -1200
		if vel.y > 1200:
			vel.y = 1200
		if vel.y < -1200:
			vel.y = -1200


func _ready():
	rng.randomize()
	position.x = world_width*rng.randf()
	position.y = 0

	vel = Vector2( v_0*(0.5-rng.randf()*2), v_0*rng.randf())

	bottom_stop = rng.randf_range(int(bottom_height*0.98), bottom_height)


func _process(delta):
	if position.y > bottom_stop:
		at_bottom = true

	# Force is water resistance value times vel^2 (allways resisting movement)
	force.x = - water_resistance*vel.x*abs(vel.x) # k*v^2
	force.y = mass*g - water_resistance*vel.y*abs(vel.y) # + mg down (duh)

	acc = force/mass

	vel += acc*delta

	if not at_bottom and not is_mouse_dragged:
		var _move = move_and_slide(vel)
	
	if is_mouse_dragged:
		pos_previous = position
		position = get_global_mouse_position()
		last_delta = delta

	if energy <= 0:
		queue_free()
	elif energy <= max_energy*0.25:
		$AnimatedSprite.set_frame(3)
	elif energy <= max_energy*0.5:
		$AnimatedSprite.set_frame(2)
	elif energy <= max_energy*0.75:
		$AnimatedSprite.set_frame(1)


func gets_eaten(bite_size):
	energy -= bite_size

func _on_Carrot_mouse_entered():
	is_mouse_on_carrot = true
func _on_Carrot_mouse_exited():
	is_mouse_on_carrot = false
