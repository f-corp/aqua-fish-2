extends Node2D

var berry_scene = preload("res://world/food/Berry.tscn")
var rng = RandomNumberGenerator.new()

var has_berry = false
var berry_start_prob = Global.berry_start_prob
var timer_min = Global.berry_growth_timer_min
var timer_max = Global.berry_growth_timer_max

func _ready():
	rng.randomize()
	if rng.randf() < berry_start_prob:
		add_berry()
	else:
		berry_removed()


# func _process(_delta):
# 	pass

	
func add_berry():
	var berry_instance = berry_scene.instance()
	add_child(berry_instance)
	has_berry = true


func berry_removed():
	has_berry = false
	
	var new_berry_time = rng.randf_range(timer_min, timer_max)
	var timer = Timer.new()

	timer.connect("timeout",self,"_on_timer_timeout") 
	timer.set_wait_time(new_berry_time)
	timer.one_shot = true
	add_child(timer)
	timer.start()


func _on_timer_timeout():
	add_berry()

