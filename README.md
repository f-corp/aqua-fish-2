# Aqua-fish 2
v1.0.1

Aqua Fish is back! 
The sequel to the critically acclaimed nextgen oceanic wildlife simulator Aqua Fish. Aqua Fish 2 features more water, more fishes, new engine (Godot) and evolutionary systems. 
Mostly just look at fishes when they interact with food, each other and them self.

## Link to Idea-presentation
https://docs.google.com/presentation/d/1ZWVFjAuC3F_pF2BWc8QlrIvpLRu2gCVDZlTOuiW_g8c/edit?usp=sharing

## Link to project trello
https://trello.com/b/3C7CDplM/aqua-fish-2
